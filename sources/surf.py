# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>Le surf</h1>
<h3>Son histoire</h3>
<p>Dès les années 1920, certains militent pour l’entrée du surf au programme olympique, à l’image de Duke
Kahanamoku, un athlète hawaïen triple champion olympique de nage libre, et un des pionniers du surf
 mondial. Bien plus tard, le surf finit par accéder au programme des Jeux à l’occasion des Jeux de Tokyo 
 2020. Il sera également présent à Paris 2024, alors que Tahiti et son spot mythique de Teahupo’o ont été
 choisis pour accueillir les épreuves.
 </p>
 <h3>Ces règles et informations</h3>
 <p>Les surfeurs effectuent des manœuvres et des figures sur une vague, et sont ensuite notés par cinq 
 juges en fonction de la variété de leur enchaînement, du type de figures réalisées et de leur difficulté.
 La vitesse, la puissance et le flow des surfeurs entrent également en ligne de compte dans les notes
 délivrées par les juges. La planche retenue pour les Jeux Olympiques est le shortboard ; une planche
 plus rapide et maniable du fait de sa taille plus réduite que celle d’un longboard, et favorisant les 
 figures spectaculaires.
 <h3>Son pictogramme</h3>
<img src="Image/surf.webp"/>

</body>
</html>"""

print(html)