# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>Le waterpolo</h1>
<h3>Son histoire</h3>
<p>Le water-polo est un des sports collectifs les plus anciens des Jeux Olympiques de l’ère moderne,
 puisqu’il fait son entrée au programme olympique en même temps que le rugby en 1900, d’abord sous la 
 forme d’une compétition entre clubs, puis d’un tournoi entre pays à partir de 1908. Les femmes
 n’accèderont cependant aux Jeux Olympiques qu’un siècle plus tard à partir des Jeux de Sydney, en 2000.</br>
 Les pays européens exercent une domination sans partage sur le palmarès olympique masculin du water-polo,
 ne laissant échapper aucun titre depuis 1908. La Hongrie, domine principalement ce palmarès, avec un 
 total impressionnant de neuf titres olympiques. Le palmarès féminin ne se dirige pas vers une telle
 hégémonie du vieux continent, puisqu’en six éditions les Etats-Unis ont remporté les trois derniers
 tournois à Londres en 2012, Rio en 2016 et Tokyo en 2021.
 </p>
 <h3>Ces règles et informations</h3>
 <p>Le water-polo voit deux équipes de sept joueurs s’affronter dans une piscine dont les dimensions
 peuvent varier entre 20x10m et 30x20m (la FINA demande des dimensions de 30x20m pour les matchs masculins,
 et 25x20m pour les matchs féminins). Les matchs sont joués en
 quatre périodes de huit minutes chacune. Les joueurs, à l’exception du gardien, ne peuvent toucher le 
 ballon que d’une main. Une possession dure 30 secondes : si l’équipe n’a pas attaqué au bout de ces 30
 secondes, le ballon passe à l’adversaire.</br>
 Le water-polo est un sport physique où les contacts sont autorisés avec les joueurs qui ont le ballon.
 Le physique, l’endurance et la puissance sont donc d’une importance capitale pour les joueurs, ainsi 
 qu’un certain sens tactique comme dans chaque sport collectif.
 <h3>Son pictogramme</h3>
<img src="Image/waterpolo.webp"/>

</body>
</html>"""

print(html)