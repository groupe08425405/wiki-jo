# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>Trampoline</h1>
<h3>Son histoire</h3>
<p>Le trampoline est devenu une discipline olympique aux Jeux Olympiques de Sydney, en 2000. Le trampoline
 compte une épreuve individuelle masculine et une épreuve féminine, et rejoint les autres disciplines
 issues de la gymnastique au programme olympique, à savoir la gymnastique rythmique et la gymnastique
 artistique.</br>
 Depuis l’arrivée de la discipline aux Jeux, la Chine a montré sa domination au palmarès olympique en
 obtenant quatorze médailles sur trente-six possibles, ainsi que quatre titres sur dix possibles, et
 mène assez largement le tableau des médailles du trampoline. Malgré cette domination chinoise, Rosie
 MacLennan (sur la photo), une athlète canadienne, est la première trampoliniste hommes et femmes
 confondus à avoir conservé son titre olympique, remportant deux titres en 2012 et 2016.
 </p>
 <h3>Ces règles et informations</h3>
 <p>Le trampoline comporte deux compétitions individuelles (une féminine et une masculine), pendant
 lesquelles les athlètes rebondissent à plus de huit mètres de haut sur une toile, pour effectuer des
 enchaînements de figures.</br>
 Le trampoline est composé d’une toile rectangulaire, en tissu synthétique. La toile est fixée à l’aide
 de ressorts en acier à un cadre, ce qui permet de faire rebondir les athlètes à des hauteurs 
 impressionnantes. Durant la compétition, les athlètes doivent effectuer des enchaînements de dix
 figures lors desquels la difficulté, l’exécution des figures et le temps passé en l’air par l’athlète
 sont évalués. Cette discipline demande une technique et une précision irréprochables.
 <h3>Son pictogramme</h3>
<img src="Image/trampoline.webp"/>

</body>
</html>"""

print(html)