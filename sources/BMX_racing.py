# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>Le BMX racing</h1>
<h3>Son histoire</h3>
<p>Le BMX racing est au programme des Jeux Olympiques depuis les Jeux de Pékin, en 2008.
 Du fait de la jeunesse de ce sport au niveau olympique, on ne peut pas réellement dire qu’un pays domine
 les débats, cependant certains athlètes se sont déjà taillé de très beaux palmarès. La Colombienne Mariana
 Pajon, tout comme le Letton Maris Strombergs, se sont par exemple adjugé deux titres olympiques chacun.
 </p>
 <h3>Ces règles et informations</h3>
 <p>Le BMX racing est beaucoup plus nerveux dans son format que les disciplines classiques du cyclisme.
 Quand ces dernières représentent souvent des efforts d’endurance durant plusieurs minutes, le BMX racing
 est un effort court et intense de quelques dizaines de secondes. Les participants sont par huit sur une
 piste mêlant sauts, virages et obstacles, où le plus rapide et réactif l’emporte.</br>
Les coureurs s’élancent d’une butte de huit mètres de hauteur sur une piste de 400m de long, et peuvent 
atteindre les 60 km/h. La réactivité et l’explosivité des athlètes sont primordiales pour jaillir en tête
 de la grille de départ, et ensuite conserver la tête de la course. Le final de la course est généralement
 haletant et passionnant à suivre pour le public. 
 <h3>Son pictogramme</h3>
<img src="Image/BMX_racing.webp"/>

</body>
</html>"""

print(html)