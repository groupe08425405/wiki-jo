# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>Le canoë sprint</h1>
<h3>Son histoire</h3>
<p>Le canoë sprint (course en ligne) est entré au programme aux Jeux de Berlin, en 1936, après avoir été 
sport de démonstration lors des Jeux de Paris de 1924. Les femmes ont participé pour la première fois à 
Londres en 1948, en kayak uniquement à l’époque.
 </p>
 <h3>Ces règles et informations</h3>
 <p>Le canoë-kayak comporte deux disciplines très différentes qui figurent toutes deux au programme
 olympique : Le canoë-kayak sprint et le canoë-kayak slalom. Chaque discipline fait référence à deux
 types d’embarcations : le kayak et le canoë. En kayak, l’athlète est en position assise et utilise
 une pagaie double, tandis qu’en canoë il est agenouillé dans l’embarcation et utilise une pagaie simple.</br>
 Les épreuves de canoë-kayak sprint se déroulent sur un bassin d’eau calme, comme les épreuves d’aviron.
 La course se fait en ligne sur huit couloirs, l’objectif étant de franchir la ligne en premier. 
 Les épreuves se disputent sur des distances de 200 m, 500 m et 1 000 m. Il existe des épreuves olympiques 
 pour les athlètes individuels (canoë simple – C1 et kayak simple – K1), les paires (canoë double – C2 et
 kayak double – K2) et les quatre (kayak quatre places – K4).
 <h3>Son pictogramme</h3>
<img src="Image/canoe_sprint.webp"/>

</body>
</html>"""

print(html)