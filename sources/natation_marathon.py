# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>La natation marathon</h1>
<h3>Son histoire</h3>
<p>La natation marathon est la dernière discipline des sports aquatiques à faire son entrée au programme 
Olympique. L’épreuve du 10 km a été ajoutée aux Jeux Olympiques de Pékin, en 2008. Du fait de la jeunesse
 de cette discipline, peu de pays ou athlètes ont pu se démarquer en natation marathon.</br>
 Si certains athlètes se consacrent uniquement à la natation marathon, certains étaient des spécialistes
 d’autres épreuves de natation marathon pratiquée en piscine, apportant une réelle concurrence aux
 spécialistes. A titre d’exemple, Oussama Mellouli, un nageur tunisien, a décroché la médaille d’or du 
 1500m nage libre à Pékin en 2008, avant de remporter quatre ans plus tard le titre de nage marathon à
 Londres.
 </p>
 <h3>Ces règles et informations</h3>
 <p>Cette discipline se déroule en eau libre ; c’est-à-dire dans la mer, une rivière ou encore un lac. 
 Le parcours est d’une longueur totale de 10km, que les athlètes mettent près de deux heures à parcourir
 ; leur endurance, leur puissance ainsi que leur mental sont soumis à rude épreuve.</br>
 La capacité d’adaptation est aussi cruciale dans cette discipline. Les marées et courants peuvent très 
 rapidement évoluer en mer, et il faut en tenir compte dans la trajectoire que l’athlète va choisir.
 Adopter une tactique adaptée au parcours et aux conditions est donc primordial. Dans les trois derniers
 kilomètres, les nageurs entrent dans la « ligne droite finale », où la gestion des efforts devient
 primordiale et fait la différence pour le classement final.
 <h3>Son pictogramme</h3>
<img src="Image/natation_marathon.webp"/>

</body>
</html>"""

print(html)