# -*- coding: utf-8 -*-
"""
Created on Tue Jan 16 14:21:24 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body class="JO">
	<h1 class="titre_debut"> Bienvenue sur ce site sur les JO 2024.</h1>
    <p>Ce site aura pour objectif de vous instruire sur JO de Paris à venir.</p>
    <p>Il sera partager en deux parties, une pour les Jeux Paralympique et une autre sur les Jeux Olympique.</p>
    <p>Bonne lecture !!!</p>
   <div class="button-container">
    <form method="post" action="olympique.py">
        <button type="submit" name="inscript">Olympique</button>
    </form>
    <form method="post" action="paralympique.py">
        <button type="submit" name="inscript">Paralympique</button>
    </form>
</div>

</body>
</html>"""

print(html)