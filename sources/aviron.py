# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>L'aviron</h1>
<h3>Son histoire</h3>
<p>L’aviron est au programme des Jeux Olympiques depuis la première édition des Jeux modernes,
 à Athènes en 1896 (épreuves masculines uniquement), les femmes feront leur entrée au programme
 officiel des Jeux Olympiques bien plus tard, en 1976, à l’occasion des Jeux de Montréal.
 Les épreuves, ayant lieu en mer, furent cependant annulées à cause des mauvaises conditions 
 météorologiques.</br>
 Les Etats-Unis dominent dans un premier temps la discipline, avant que l’URSS et l’Allemagne occupent
 le devant de la scène. C’est cependant un athlète anglais, Sir Steve Redgrave, qui est considéré comme 
 le plus grand rameur de l’histoire, puisqu’il a remporté cinq médailles d’or lors de cinq éditions des 
 Jeux Olympiques. Chez les femmes, c’est Elisabeta Oleniuc-Lipa, une athlète roumaine, qui peut 
 revendiquer ce titre du haut de ses cinq titres olympiques, deux médailles d’argent et une de bronze.
 </p>
 <h3>Ces règles et informations</h3>
 <p>L’aviron consiste en une embarcation propulsée à la force de rames fixées au bateau.
 Ce sport a la particularité de voir les athlètes se positionner dos à la direction dans
 laquelle ils se déplacent, et donc de passer la ligne d’arrivée de dos.</br>
 Le format de compétition en aviron se dispute en ligne sur une distance de 2000 mètres 
 en embarcation individuelle ou en équipage composé de deux, quatre ou huit rameurs. 
 Il existe deux disciplines, l’aviron de couple et l’aviron de pointe. Alors qu’en pointe, 
 le rameur manie une seule rame de ses deux mains, en couple le rameur manie une rame dans
 chaque main. Lorsque l’équipage comporte 8 rameurs, un barreur dirige l’équipe et gouverne 
 l’embarcation. Le bateau dispose d’un petit gouvernail qui permet de diriger l’embarcation,
 celui-ci est relié par un câble au pied de l’un des rameurs qui le manie. Il existe par ailleurs 
 deux épreuves de poids légers.
 </p>
 <h3>Son pictogramme</h3>
 <img src="Image/aviron.webp"/>
</body>
</html>"""

print(html)