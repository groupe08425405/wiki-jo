# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>Le basketball 3x3</h1>
<h3>Son histoire</h3>
<p>En 2010, le basketball 3×3 intègre le programme des Jeux Olympiques de la Jeunesse, à Singapour. 
L’expérience est reconduite en 2014 pour l’édition de Nanjing ainsi qu’en 2018 à Buenos Aires. 
Le succès populaire de ce jeune sport conduit le CIO à inclure en 2017 le basketball 3×3 au programme
 olympique, pour les Jeux de Tokyo 2020. Ce nouveau format dynamique, spectaculaire et urbain a tout 
 pour passionner les foules.
 </p>
 <h3>Ces règles et informations</h3>
 <p>Le basketball 3×3 se joue par équipes de trois joueurs, sur un demi-terrain ; les 
 deux équipes attaquent et défendent le même panier, en fonction de qui a la possession.
 L’équipe en tête au bout de dix minutes de jeu, ou la première à atteindre 21 points, remporte
 le match. Le principe des tirs primés hors de la ligne des trois points est conservé, mais passe
 à deux points à l’extérieur de cette ligne et un point à l’intérieur.</br>
 A Tokyo, en 2021, le tournoi olympique a vu huit équipes de trois joueurs et joueuses, plus un remplaçant,
 s’affronter pour les premiers titres olympiques de l’histoire du 3×3, qui ont été décrochés par
 la Lettonie chez les hommes, et les Etats-Unis chez les femmes. Bien que les phases de jeu soient 
 plus rapide, les qualités requises pour jouer au 3×3 sont les mêmes qu’au basketball 5×5, avec comme
 maîtres mots la polyvalence, l’adresse et la vision du jeu.
 <h3>Son pictogramme</h3>
<img src="Image/basketball_3x3.webp"/>

</body>
</html>"""

print(html)