# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>Le taekwondo</h1>
<h3>Son histoire</h3>
<p>Le taekwondo est intégré pour la première fois au programme des Jeux de Séoul, son pays d’origine en
 1988, comme sport de démonstration. Il conserve son statut de sport de démonstration à Barcelone, en 1992,
 puis disparaît du programme d’Atlanta en 1996. Finalement, quatre ans plus tard, le sport intègre
 définitivement le programme olympique à partir des Jeux de Sydney, en 2000, pendant lesquels quatre 
 compétitions masculines et féminines sont organisées.</br>
 Si la Corée, pays fondateur du taekwondo et à l’origine de l’arrivée du sport aux Jeux Olympiques, 
 dominait auparavant les compétitions, ce n’est aujourd’hui plus le cas. Beaucoup de pays figurent 
 désormais au palmarès olympique du taekwondo, Des combattants de huit pays différents ont par exemple
 remporté une médaille d’or en 2012, à Londres.</br>
 Certains pays ont marqué l’histoire en remportant la première médaille olympique de leur histoire en
 taekwondo, comme le Vietnam en 2000, l’Afghanistan en 2008, le Gabon en 2012, et le Niger et la
 Jordanie 2016, ou encore leur première médaille olympique féminine à l’image de l’Iran et de la Côte
 d’Ivoire en 2016.
 </p>
 <h3>Ces règles et informations</h3>
 <p>Au taekwondo, les mains comme les pieds sont utilisés, et le but est de donner des coups de pied ou 
 des coups de poings sans être touché soi-même. Cet art martial est basé sur une combinaison rapide de
 mouvements des jambes et des bras, et se déroule sur une aire de combat en forme d’octogone, sur trois
 rounds de deux minutes chacun. Les points sont marqués en fonction de la technicité du coup porté ; par
 exemple, un coup de pied porté à la tête vaut plus de points qu’un coup au niveau du torse, ou encore une 
 technique retournée sera valorisée par des points additionnels. Des pénalités peuvent être attribuées pour
 différentes fautes commises par les combattants.
 <h3>Son pictogramme</h3>
<img src="Image/taekwondo.webp"/>

</body>
</html>"""

print(html)