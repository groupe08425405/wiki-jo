# -*- coding: utf-8 -*-
"""
Created on Tue Jan 23 13:55:28 2024

@author: lbelair
"""

import http.server

# Choisir un port et indiquer l'IP du serveur
port = 80
address = ("", port)

# Le serveur
server = http.server.HTTPServer

# Utiliser CGIHTTPRequestHandler pour prendre en charge les scripts CGI
handler = http.server.CGIHTTPRequestHandler

# Les scripts seront dans le répertoire racine directement
handler.cgi_directories = ["/"]

# Autoriser les méthodes HTTP POST
handler.allow_methods = ["POST"]

httpd = server(address, handler)
print(f"Serveur démarré sur le PORT{port}")
httpd.serve_forever()