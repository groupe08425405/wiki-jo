# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>L'hathlétisme</h1>
<h3>Son histoire</h3>
<p>C’est fort logiquement que l’athlétisme figure au programme de tous les Jeux modernes, dès ceux de 
la première olympiade en 1896, à Athènes. Cette existence séculaire confère à l’athlétisme le statut de
 « sport roi » des Jeux Olympiques d’été, et contribue à sa popularité. Le programme masculin est presque
 le même depuis les Jeux de 1932 à Los Angeles, seul le 20km marche a été ajouté aux Jeux de Melbourne en
 1956. Les femmes participent depuis 1928 aux épreuves d’athlétisme. Le programme féminin comportait
 seulement 17 épreuves jusqu’en 1992, il est désormais quasiment équivalent à celui des hommes depuis
 l’intégration au programme féminin du 3000m steeple en 2008. Depuis 1960, la pratique de l’athlétisme 
 grandit dans les pays en développement et se développe réellement au niveau mondial.
 </p>
 <h3>Ces règles et informations</h3>
 <p>Aujourd’hui encore l’athlétisme comporte une grande variété d’épreuves. Du fait de ces nombreuses
 catégories et disciplines, l’athlétisme est le sport individuel qui comporte le plus de participants aux
 Jeux Olympiques.</br>
 Le programme sur piste contient des épreuves masculines et féminines de sprint, de demi-fond et de fond,
 ainsi que des courses de haies, de steeple et des relais. Ces épreuves se déroulent toutes sur la piste
 du stade olympique, qui est d’une longueur de 400 mètres et est composée de deux lignes droites et de deux
 demi-cercles.</br>
 Sur route, les épreuves sont de deux types ; marathon et marche athlétique, et sont extrêmement exigeantes
 en plus de revêtir un aspect tactique qui peut donner lieu à des stratégies diverses, ou même des
 alliances. Elles prennent place sur la voie publique, avec des spectateurs le long du parcours qui
 peuvent observer et encourager les coureurs.</br>
 Deux épreuves combinées sont au programme olympique ; une féminine (combinant sept épreuves, l’Heptathlon)
 et une masculine (combinant dix épreuves, le Décathlon). Ces épreuves se déroulent sur deux jours dans un 
 enchaînement qui met les athlètes à rude épreuve, pour finir par couronner le plus complet d’entre eux.</br>
 Les concours se déroulent à côté de la piste du stade olympique. Elles prennent place dans une aire de
 saut (en hauteur et à la perche), un bac à sable (pour le saut en longueur et le triple saut), un cercle
 pour les lancers (du poids, du disque et du marteau) et enfin une piste de lancer du javelot. Les athlètes
 participent chacun leur tour, et les concours comprennent une phase de qualification, au cours de laquelle
 les meilleurs athlètes se qualifient pour la finale. 
 </p>
 <h3>Son pictogramme</h3>
 <img src="Image/athletisme.webp"/>
</body>
</html>"""

print(html)