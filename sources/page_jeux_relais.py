# -*- coding: utf-8 -*-
"""
Created on Thu Feb 29 19:52:34 2024

@author: maxim
"""

import cgi # Notre script va retourner du HTML 
print("Content-type: text/html; charset=UTF_8\n")
html = """<!DOCTYPE html> <html lang="fr"> 
<!DOCTYPE html>
<html lang="en">
    <title>Jeux corespondance Images-Mots</title>
    <style>
    
        h1 {
            border : 2px solid black;
            background: black;
            color: #f5f5f5
        }
        .container {
            text-align: center;
        }

        #game-container {
            display: flex;
            justify-content: space-around;
            margin-bottom: 20px;
        }

        .image-container {
            width: 200px;
            height: 200px;
            border: 2px solid black;
            margin: 10px;
            background-size: cover; /* Redimensionne l'image pour couvrir entièrement le conteneur */
        }

        .word {
            display: inline-block;
            width: 100px;
            height: 20px;
            border: 2px solid black;
            margin: 5px;
            padding: 10px;
            cursor: pointer;
            background: #f5f5f5;
        }
        
        body {
            background-color:#f5f5f5 ;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Jeux de corespondance Images-Mots</h1>
        <div id="game-container">
            <div class="image-container" id="image1"></div>
            <div class="image-container" id="image2"></div>
            <div class="image-container" id="image3"></div>
            <div class="image-container" id="image4"></div>
            <div class="image-container" id="image5"></div>
            <div class="image-container" id="image6"></div>
        </div>
        <div id="word-container">
            <div class="word" id="word1" draggable="true">Basket-fauteuil</div>
            <div class="word" id="word2" draggable="true">Cecifoot</div>
            <div class="word" id="word3" draggable="true">Para-aviron</div>
            <div class="word" id="word4" draggable="true">Rugby-fauteuil</div>
            <div class="word" id="word5" draggable="true">Para-judo</div>
            <div class="word" id="word6" draggable="true">Para-canoe</div>
        </div>
    </div>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            const images = document.querySelectorAll('.image-container');
            const words = document.querySelectorAll('.word');

            // Array of sport objects with name and corresponding image path
            const sports = [
                { name: 'Basket-fauteuil', image: 'images/basket-fauteuil.jpg' },
                { name: 'Cecifoot', image: 'images/cecifoot.jpg' },
                { name: 'Para-aviron', image: 'images/para-aviron.jpg' },
                { name: 'Rugby-fauteuil', image: 'images/rugby-fauteuil.jpg' },
                { name: 'Para-judo', image: 'images/para-judo.jpg' },
                { name: 'Para-canoe', image: 'images/para-canoe.jpg' }
            ];

            // Function to shuffle array
            function shuffleArray(array) {
                for (let i = array.length - 1; i > 0; i--) {
                    const j = Math.floor(Math.random() * (i + 1));
                    [array[i], array[j]] = [array[j], array[i]];
                }
            }

            // Function to reset the game
            function resetGame() {
                shuffleArray(sports);
                images.forEach((image, index) => {
                    image.style.backgroundImage = `url(${sports[index].image})`;
                    image.dataset.sport = sports[index].name;
                });
            }

            // Initialize game
            resetGame();

            // Add event listeners for drag and drop
            words.forEach(word => {
                word.addEventListener('dragstart', dragStart);
                word.addEventListener('dragend', dragEnd);
            });

            images.forEach(image => {
                image.addEventListener('dragover', dragOver);
                image.addEventListener('dragenter', dragEnter);
                image.addEventListener('dragleave', dragLeave);
                image.addEventListener('drop', dragDrop);
            });

            // Functions for drag and drop
            let draggedWord = null;

            function dragStart() {
                draggedWord = this;
                setTimeout(() => this.style.display = 'none', 0);
            }

            function dragEnd() {
                draggedWord = null;
                this.style.display = 'inline-block';
            }

            function dragOver(e) {
                e.preventDefault();
            }

            function dragEnter(e) {
                e.preventDefault();
                this.style.border = 'dashed 2px black';
            }

            function dragLeave() {
                this.style.border = '2px solid black';
            }

            function dragDrop() {
                this.style.border = '2px solid black';
                if (draggedWord.textContent === this.dataset.sport) {
                    this.appendChild(draggedWord);
                    checkGameStatus();
                } else {
                    resetGame();
                }
            }

            // Function to check if all sports are matched
            function checkGameStatus() {
                let matched = true;
                images.forEach(image => {
                    if (image.children.length === 0) {
                        matched = false;
                    }
                });
                if (matched) {
                    alert('Congratulations! Vous avez trouvé tout les sports correspondant avec succès');
                    resetGame();
                }
            }
        });
    </script>
    

    
</body>
</html>"""
print(html)