# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>Le judo</h1>
<h3>Son histoire</h3>
<p>Ce sport a fait son apparition au Jeux Olympiques de Tokyo en 1964, et ne quittera plus le programme 
olympique à compter des Jeux de Munich en 1972, et le judo féminin à partir des Jeux de Barcelone en 1992.
 L’art martial est globalement dominé par le Japon, qui a remporté 96 médailles, suivi par la France 
 (57 médailles) et la Corée du Sud (46 médailles) qui s’illustrent également à chaque édition des Jeux.
 Depuis son arrivée aux Jeux Olympiques, le judo s’est largement développé à travers le monde, puisque 
 128 pays participaient en 2021 aux compétitions olympiques à Tokyo.
 </p>
 <h3>Ces règles et informations</h3>
 <p>L’objectif est de projeter son adversaire au sol, de l’immobiliser ou de l’obliger à abandonner en 
 utilisant des clés articulaires et des étranglements. Il existe deux principaux types d’avantages dans
 le judo moderne. Le Ippon consiste en un impact significatif sur le dos avec force, vitesse et contrôle, 
 ou un abandon (causé par un étranglement ou une clef de bras), ou encore une immobilisation au sol de 20
 secondes. Le Ippon donne immédiatement la victoire à celui qui l’inflige à son adversaire. Le waza-ari,
 lui, fait suite à un impact pour lequel il manque l’un des trois critères du Ippon, ou une immobilisation
 inférieure à 20 secondes (mais supérieure à dix secondes). Deux waza-ari équivalent à un Ippon et donnent
 la victoire à celui qui les exécute.</br>
 Le judo est divisé en catégories de poids pour les femmes et les hommes, avec des combats de quatre
 minutes, donnant lieu à une prolongation si le score est à égalité. Le judo prônant un enseignement de 
 valeurs morales, des pénalités peuvent être données aux combattants trop passifs ou adoptant un 
 comportement « contraire à l’esprit du judo ».
 <h3>Son pictogramme</h3>
<img src="Image/judo.webp"/>

</body>
</html>"""

print(html)