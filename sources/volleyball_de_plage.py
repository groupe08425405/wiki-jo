# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>Le volleyball de plage</h1>
<h3>Son histoire</h3>
<p>Le volleyball de plage a fait ses débuts olympiques officiels aux Jeux d’Atlanta en 1996, après une 
apparition en tant que sport de démonstration en 1992, à Barcelone. Le Brésil et les Etats-Unis se sont 
taillés les plus gros palmarès de ce sport, en se partageant 24 médailles (dont 10 en or) sur 42 possibles
 Les seuls autres pays a avoir remporté l’or dans ce sport sont l’Allemagne dans le tournoi masculin à 
 Londres en 2012 et dans le tournoi féminin à Rio en 2016, l’Australie chez les femmes à domicile en 2000,
 et la Norvège chez les hommes à Tokyo en 2021.
 </p>
 <h3>Ces règles et informations</h3>
 <p>Le volleyball de plage oppose des équipes de deux de chaque côté du filet, qui s’affrontent en deux
 sets gagnants sur un terrain de sable de seize mètres de long sur huit mètres de large, ce qui est 
 légèrement plus petit que le terrain en salle. Le filet, lui, est à la même hauteur qu’en salle 
 (2,24m pour les femmes, et 2,43m pour les hommes).</br>
 Les matchs sont disputés en deux sets gagnants, les deux premiers se jouant en 21 points gagnants, et
 l’éventuel troisième set en quinze. Les équipes étant de seulement deux joueurs, les espaces sont
 beaucoup plus grands sur le terrain, et demandent donc d’excellents réflexes de la part des joueurs. 
 Les matchs se jouant en extérieur, il faut également aux joueurs de grandes capacités d’adaptabilité, 
 puisque le vent, le soleil ou encore la pluie peuvent influencer les conditions de jeu.
 <h3>Son pictogramme</h3>
<img src="Image/volleyball_de_plage.webp"/>

</body>
</html>"""

print(html)