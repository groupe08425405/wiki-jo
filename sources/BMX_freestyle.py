# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>Le BMX freestyle</h1>
<h3>Son histoire</h3>
<p>Pour sa première incursion dans le monde de l’olympisme, le BMX freestyle est apparu au programme des
 Jeux Olympiques de la Jeunesse de Buenos Aires, en 2018. Un spectacle impressionnant et un public nombreux 
 et enjoué ont convaincu le CIO du potentiel de ce sport urbain par excellence.</br>
 En 2021, c’est aux Jeux Olympiques de Tokyo que le BMX freestyle a fait ses grands débuts. Ce nouveau 
 sport apporte sa jeunesse, son adrénaline et ses spines vertigineux à un programme de cyclisme dense
 et excitant. 
 </p>
 <h3>Ces règles et informations</h3>
 <p>Les épreuves de BMX freestyle, au programme des Jeux Olympiques de Tokyo, se déroulent dans un park,
 où les riders ont exécuté un maximum de figures en deux runs de 60 secondes, qui ont été notés sur leur
 difficulté, l’amplitude du saut ou encore la créativité et le style de l’enchaînement.
 <h3>Son pictogramme</h3>
<img src="Image/BMX_freestyle.webp"/>

</body>
</html>"""

print(html)