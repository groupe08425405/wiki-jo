# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>La natation artistique</h1>
<h3>Son histoire</h3>
<p>La natation artistique intègre le programme olympique en 1984 aux Jeux de Los Angeles. A Paris 2024,
 pour la première fois des hommes seront autorisés à participer dans le cadre de l’épreuve par équipes.</br>
 Le palmarès olympique a dans un premier temps été dominé par les pays d’Amérique du Nord, les Etats-Unis 
 et le Canada cumulant huit titres olympiques, soit la moitié des titres possibles. La Russie s’est
 ensuite largement imposée sur le devant de la scène olympique, remportant à elle seule les douze titres
 mis en jeu depuis les Jeux de Sydney, en 2000.
 </p>
 <h3>Ces règles et informations</h3>
 <p>Deux épreuves figurent au programme olympique ; une en duo, et une en équipe de huit athlètes. Chaque 
 épreuve comporte deux représentations, un programme libre et un programme technique. Lors des programmes,
 des juges notent l’exécution, la synchronisation, la difficulté, l’utilisation de la musique et la 
 chorégraphie. Les programmes sont exécutés dans une piscine à trois mètres de profondeur, 25m de longueur
 et 20m de largeur.</br>
 Les nageuses ont besoin, au cours de leurs enchaînements, de se maintenir et de se propulser hors de l’eau
 ou encore de pivoter avec la moitié supérieure de leur corps immergée… la discipline sollicite énormément
 de souplesse, de puissance, de rigueur et de coordination chez les athlètes.
 <h3>Son pictogramme</h3>
<img src="Image/natation_artistique.webp"/>

</body>
</html>"""

print(html)