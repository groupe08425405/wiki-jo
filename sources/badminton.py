# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>Le Badminton</h1>
<h3>Son histoire</h3>
<p>A l’origine, le badminton fait son apparition aux Jeux Olympiques en tant que sport de démonstration
 aux Jeux Olympiques de 1972, à Munich. En 1988, il réapparaît en tant que sport de démonstration à Séoul,
 puis il fait enfin son entrée officielle au programme olympique en 1992, aux Jeux de Barcelone.
 Des épreuves de simple et de double hommes et femmes sont organisées, et une épreuve de double mixte
 est ajoutée au programme dès les Jeux Olympiques d’Atlanta en 1996. Les nation asiatiques dominent
 la liste des médaillés, les athlètes de ce continent se partageant 106 des 121 médailles possibles dans
 l’histoire olympique du badminton.</br>
 Pour Paris 2024, 172 athlètes participeront aux cinq épreuves de badminton, qui délivreront au cours
 des dix jours de compétition cinq médailles d’or, d’argent et de bronze
 </p>
 <h3>Ces règles et informations</h3>
 <p>Le badminton est un sport de raquette qui se pratique en intérieur, et oppose deux joueurs
 (en simple) ou deux paires (en double) sur un terrain divisé en deux parties par un filet.
 Ce n’est pas une balle mais un volant que les joueurs s’échangent ; un projectile en 
 forme de cône et cerclé de plumes. Les matchs se jouent en deux sets gagnants, et les sets
 se jouent en 21 points.</p>
 <h3>Son pictogramme</h3>
<img src="Image/badminton.webp"/>

</body>
</html>"""

print(html)