# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>Le rugby</h1>
<h3>Son histoire</h3>
<p>L’histoire du rugby avec les Jeux Olympiques s’étale sur plusieurs périodes. Une première, vers la
 rénovation des Jeux Olympiques où il se pratiquait à quinze, puis plus récemment dans sa variante à sept.
 En effet, en 1900, 1908, 1920 et 1924, un tournoi à quinze masculin figure au programme olympique : il
 voit s’imposer successivement l’équipe de France, l’Australasie (équipe unissant l’Australie et la
 Nouvelle-Zélande), puis les Etats-Unis par deux fois.</br>
 Depuis les Jeux de Rio, en 2016, le rugby fait son grand retour sur la scène olympique, cette fois dans
 sa formule à sept joueurs sur le terrain, pour un tournoi féminin (remporté par la Nouvelle-Zélande en 
 2021) et un tournoi masculin (remporté par les Fidji en 2021).
 </p>
 <h3>Ces règles et informations</h3>
 <p>Si de nombreuses variantes du rugby se sont développées au fil des années, comme le rugby à treize,
 le beach rugby ou encore le flag rugby, les formules principalement jouées sont le rugby à quinze et le 
 rugby à sept. Toutes les variantes conservent une base de règles communes, codifiant les plaquages, les 
 passes strictement jouées vers l’arrière, ou encore le déroulement des mêlées, qui évolue en fonction du 
 nombre de joueurs sur le terrain. La particularité d’un rugby joué à sept joueur fait que le jeu est plus
 rapide, et intense : les matchs durent quatorze minutes, mais sont extrêmement exigeants et comprennent 
 plus de sprints et d’essais qu’un match de rugby à quinze. Les manières d’inscrire des points sont les
 mêmes qu’au rugby à quinze : un essai vaut cinq points, une transformation deux supplémentaires, un drop
 et une pénalité valent également trois points.
 <h3>Son pictogramme</h3>
<img src="Image/rugby.webp"/>

</body>
</html>"""

print(html)