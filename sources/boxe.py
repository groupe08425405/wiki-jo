# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>La boxe</h1>
<h3>Son histoire</h3>
<p>La boxe a fait ses débuts olympiques en 1904, et a depuis été disputée à chaque édition, à
 l’exception des Jeux de 1912 à Stockholm car la loi suédoise interdisait la pratique de ce sport
 à l’époque. Les épreuves féminines ont été introduites au programme olympique pour la première fois
 en 2012, à Londres. Pour la première édition à Saint Louis en 1904, les Etats-Unis ont remporté toutes 
 les médailles possibles puisque il n’y avait que des américains pour participer aux épreuves de boxe !
 Les Etats-Unis ont ensuite continué à être performants en boxe puisqu’ils ont remporté 117 médailles. 
 Les combattants de Cuba ne sont pas en reste avec 78 médailles, alors que la Grande-Bretagne en compte 62.
 </p>
 <h3>Ces règles et informations</h3>
 <p>Les règles olympiques de la boxe sont particulières. Si jusqu’aux Jeux de Londres les participants
 devaient être amateurs, ce qui a souvent fait des Jeux Olympiques le point de départ d’une grande 
 carrière pour de grands boxeurs, à l’image de Cassius Clay (Mohamed Ali), depuis les JO de Rio en 
 2016 les professionnels peuvent prétendre à la qualification.</br>
 De 1984 à 2012, les participants doivent porter un casque de protection, les différenciant des pratiquants
 professionnels. Une règle abandonnée lors des Jeux de Rio 2016. Un combat se déroule en trois rounds 
 de trois minutes pour les hommes comme pour les femmes, qui conservent cependant le casque de protection. 
 et quatre rounds de deux minutes pour les femmes. Le combattant doit toucher son adversaire sur les zone
 autorisées, et le jugement se fait round par round.</br>
 A la fin de chaque round, chacun des juges détermine un vainqueur pour le round, en se basant sur 
 plusieurs critères, et le vainqueur remporte 10 points pour le round. Le perdant se voit attribué
 entre sept et neuf points par round en fonction de son niveau de performance dans le round. A la 
 fin du combat, chacun des juges additionne le score des rounds pour déterminer le vainqueur final.
 <h3>Son pictogramme</h3>
<img src="Image/boxe.webp"/>

</body>
</html>"""

print(html)