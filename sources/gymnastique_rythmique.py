# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>La gymnastique rythmique</h1>
<h3>Son histoire</h3>
<p>En 1984 à Los Angeles, la gymnastique rythmique, toujours aujourd’hui réservée aux femmes, fait son
 apparition au programme olympique avec une épreuve individuelle. Douze ans plus tard à Atlanta, en 1996,
 une épreuve par ensembles est ajoutée au programme.</br>
Au palmarès olympique, les pays d’Europe de l’Est, et en particulier la Russie, sont très dominateurs ;
 la Russie a remporté dix titres sur quinze possibles depuis l’introduction de la discipline au programme 
 olympique. Seuls l’Espagne et le Canada ont réussi à remporter un titre face à l’hégémonie des pays
 d’Europe de l’Est sur la gymnastique rythmique.
</p>
 <h3>Ces règles et informations</h3>
 <p>La gymnastique rythmique est une discipline qui s'approche d'un mélange entre danse classique et
 gymnastique artistique, regroupant le maniement de quatres engins d'adresse, le cerceau, le ballon, les
 massues et le ruban. Pour accompagner les athlètes, la musique choisie joue un rôle important, et les
 morceaux avec chant sont permis. Cette discipline est réservée aux femmes.</br>
 L’épreuve individuelle voit les gymnastes se produire à quatre reprises, en utilisant un engin différent
 à chaque fois. Les présentations doivent durer entre 75 et 90 secondes.  La seconde épreuve, 
 par ensembles, voit des équipes de cinq gymnastes se produire deux fois, en utilisant cette fois plusieurs
 engins en même temps. Dans cette épreuve, les présentations doivent durer entre deux minutes et quinze
 secondes et deux minutes et trente secondes. Des pénalités sont appliquées si ces durées ne sont pas
 respectées. Les présentations sont évaluées en combinant une note D (contenu / difficulté), une note A
 (artistique) et une note E (exécution).

 <h3>Son pictogramme</h3>
<img src="Image/gymnastique_rythmique.webp"/>

</body>
</html>"""

print(html)