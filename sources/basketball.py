# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>Le basketball</h1>
<h3>Son histoire</h3>
<p>Le basketball fait son entrée aux Jeux Olympiques en tant que sport de démonstration. 
Lors des Jeux de 1904 à Saint Louis, la compétition consiste en une épreuve du championnat
 américain, et oppose uniquement des équipes américaines. Plus tard, le basketball devient
 un sport olympique à part entière lors des Jeux Olympiques de Berlin, en 1936. Le basketball
 féminin rejoint son homologue masculin au programme olympique 40 ans plus tard, lors des Jeux 
 de 1976 à Montréal.</br>
 Historiquement, les Etats-Unis se montrent particulièrement dominateurs dans ce sport qu’ils ont inventé
 : les hommes ont remporté l’or olympique de toutes les éditions sauf en 1972, 1980, 1988 et 2004, et
 les femmes ont gagné chaque édition depuis 1984, à l’exception du tournoi de 1992. Les Etats-Unis ont
 également marqué le tournoi olympique de Barcelone en 1992, en envoyant aux Jeux une « Dream Team », 
 composée de stars de la NBA. Cette dernière remporte la médaille d’or, en gagnant ses matchs avec une
 moyenne de plus de 40 points d’écart avec leurs adversaires.
 </p>
 <h3>Ces règles et informations</h3>
 <p>Le basketball oppose deux équipes de cinq joueurs sur un terrain rectangulaire,
 en intérieur. Il se pratique en maniant le ballon à la main, et le but est de marquer
 un plus grand nombre de points que l’adversaire, en envoyant le ballon à travers un panier
 placé à 3,05m du sol, en marquant des paniers à deux et trois points en fonction de la distance
 de tir. En format olympique, le basketball se joue en quatre quarts-temps de dix minutes. Les joueurs 
 enchaînent actions offensives et défensives dans ce sport qui demande endurance, polyvalence et bien 
 sur une grande adresse aux athlètes pour enchaîner les courses et les paniers.
 <h3>Son pictogramme</h3>
<img src="Image/basketball.webp"/>

</body>
</html>"""

print(html)