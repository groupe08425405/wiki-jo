# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>Le pentathlon moderne</h1>
<h3>Son histoire</h3>
<p>Le pentathlon moderne a été inclus au programme olympique pour la première fois en 1912, sous l’impulsion 
du Baron Pierre de Coubertin. Initialement les épreuves se déroulaient au rythme d’une par jour, avant
 d’être rassemblées la même journée, pour plus de dynamisme. L’épreuve féminine a été ajoutée au programme
 olympique à Sydney, en 2000.  Les pays d’Europe ont longtemps été dominateurs aux Jeux, et notamment la
 Hongrie et la Suède, la Suède remportant notamment 13 médailles sur 15 entre les Jeux de Stockholm en
 1912, et ceux de Los Angeles en 1932.
 </p>
 <h3>Ces règles et informations</h3>
 <p>La formule moderne du pentathlon combine quatre épreuves, issues de cinq sports :</br>
-La natation, avec un 200m nage libre ;</br>
-L’escrime, avec premièrement un tournoi de classement sous la forme de matchs d’une minute contre tous les autres athlètes. En fonction du classement de ce tournoi, les athlètes disputent ensuite une épreuve d’escrime bonus, qui se déroule sous la forme d’un tournoi à élimination directe. Les combats durent 30 secondes, et chaque victoire apporte un point bonus au score de l’athlète à l’épreuve de classement.</br> 
-L’équitation, avec un parcours de saut d’obstacles sur un cheval que l’athlète ne connaît pas et qu’il a tiré au sort vingt minutes avant le début de l’épreuve ;</br>
-La course à pied et le tir, avec un laser-run où les athlètes enchaînent phases de course à pied et phases de tir sur cinq cibles, à une distance de dix mètres.</br>
Le pentathlon moderne se dispute en deux temps ; premièrement, les athlètes marquent des points en fonction
 de leur classement lors des épreuves d’escrime, d’équitation et de natation, qui conditionnent leur
 ordre de départ lors du laser-run. Lors du laser-run, les athlètes partent avec un retard correspondant
 au nombre de points d’écart qu’ils ont avec le leader du classement. Le premier athlète à franchir la
 ligne d’arrivée du laser run remporte la médaille d’or. Du fait de la diversité des épreuves qui le
 composent, le pentathlon moderne est un sport extrêmement exigeant, qui requiert aux athlètes une grande
 force mentale et physique, ainsi qu’une polyvalence hors du commun.
 <h3>Son pictogramme</h3>
<img src="Image/pentathlon_moderne.webp"/>

</body>
</html>"""

print(html)