# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>Plongeon</h1>
<h3>Son histoire</h3>
<p>La discipline fait rapidement son entrée au programme olympique, à partir de 1904, aux Jeux de Saint 
Louis. En 1912, les femmes entrent au programme olympique de cette discipline. Les épreuves synchronisées
 sont ajoutées au programme olympique en 2000, à l’occasion des jeux de Sydney.</br>
 A partir des Jeux de Saint Louis en 1904, les Etats-Unis dominent la discipline. Aujourd’hui, c’est 
 la Chine qui a pris les devants et qui s’impose sur la scène olympique, remportant par exemple 12 
 médailles sur 24 possibles en 2021 à Tokyo, dont sept médailles d’or.
 </p>
 <h3>Ces règles et informations</h3>
 <p>Au programme olympique, deux types d’épreuves de plongeon sont proposées : d’une part le tremplin
 à trois mètres, et la plateforme de haut-vol, fixée à dix mètres de haut. L’épreuve à trois mètres
 utilise un tremplin qui permet aux athlètes de rebondir et de gagner en hauteur, alors que pour le
 haut-vol plateforme, le départ se fait sur une plateforme fixe et rigide à dix mètres de haut. Des
 compétitions individuelles et synchronisées sont organisées à trois mètres et en haut-vol.</br>
 Les sauts sont évalués en fonction de plusieurs critères, dont la beauté des mouvements, leur complexité, 
 ainsi que la qualité de l’entrée dans l’eau des plongeurs. Lors des épreuves synchronisées, l’harmonie
 entre les deux athlètes entre également en ligne de compte.
 <h3>Son pictogramme</h3>
<img src="Image/plongeon.webp"/>

</body>
</html>"""

print(html)