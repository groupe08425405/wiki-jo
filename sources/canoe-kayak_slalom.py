# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>Le canoë-kayak slalom</h1>
<h3>Son histoire</h3>
<p>Le canoë slalom est apparu pour la première fois aux Jeux Olympiques de Munich, en 1972, et est 
définitivement entré au programme olympique aux Jeux de 1992 à Barcelone. Le palmarès olympique du 
slalom est largement dominé par l’Europe, qui s’arroge 90% des médailles décernées. Par exemple, entre
 les éditions d’Atlanta en 1996 et de Rio en 2016, seules trois médailles masculines ont échappé aux 
 nations européennes.</br>
 À Paris, un nouveau format de compétition de slalom fera ses débuts avec des épreuves de kayak cross
 (hommes et femmes). A l’instar du ski cross ou du snowboard cross, quatre athlètes s’élanceront ensemble 
 d’une rampe de départ et devront descendre un parcours délimité par dix portes ou obstacles nécessitant 
 une manœuvre de type kayak-rail. Les deux premiers athlètes qui franchissent la ligne d’arrivée sans 
 manquer une porte passent à la phase suivante.
 
 <h3>Ces règles et informations</h3>
 </p>Le canoë-kayak comporte deux disciplines très différentes qui figurent toutes deux au programme 
 olympique : Le canoë-kayak sprint et le canoë-kayak slalom.  Chaque discipline fait référence à deux 
 types d’embarcations : le kayak et le canoë. Le « pagayeur » de kayak est en position assise et utilise
 une pagaie double, tandis que le « pagayeur » de canoë est agenouillé dans l’embarcation et utilise une
 pagaie simple.</br>
 Les compétitions de canoë-kayak slalom se dérouleront sur le tout nouveau stade d’eau vive. 
 La compétition de slalom est une course chronométrée sur un parcours comportant jusqu’à 25 portes à
 franchir. Toucher une porte ajoute une pénalité de deux secondes à la course et manquer une porte entraîne
 une pénalité de 50 secondes, ce qui annihile toute chance de l’athlète d’obtenir un bon résultat.
 <p>
 <h3>Son pictogramme</h3>
<img src="Image/canoe-kayak_slalom.webp"/>

</body>
</html>"""

print(html)