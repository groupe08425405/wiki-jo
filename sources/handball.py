# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>Le handball</h1>
<h3>Son histoire</h3>
<p>Aux Jeux Olympiques, le handball est introduit pour la première fois aux Jeux de Berlin en 1936,
 dans sa version sur gazon, et est ensuite disputé en tant que sport de démonstration aux Jeux de 1952
 à Helsinki. Il réapparait au programme olympique vingt ans plus tard aux Jeux de Munich en 1972, dans
 sa formule en salle cette fois-ci.</br>
 L’épreuve féminine arrive au programme olympique quatre ans plus tard, à Montréal. Le palmarès olympique 
 du handball est largement dominé par l’Europe. Tous les titres olympiques ont été remportés par une nation
 européenne, à l’exception des deux victoires de la Corée du Sud à Séoul en 1988, puis à Barcelone en
 1992 dans le tournoi féminin.  
 </p>
 <h3>Ces règles et informations</h3>
 <p>La version du handball aujourd’hui se joue sur un terrain de 40m x 20m en salle qui voit s’affronter
 deux équipes de sept joueurs, qui peuvent dribbler le ballon à la main au maximum tous les trois appuis,
 et garder le ballon en main au maximum pendant trois secondes. L’équipe qui, au terme des deux mi-temps
 de 30 minutes, a marqué le plus de buts, l’emporte. Les tournois olympiques féminins et masculins sont 
 composés de douze équipes.</br>
 Le handball est un sport de contact qui autorise les contacts entre joueurs offensifs et défensifs ; en 
 résulte un jeu très physique et éprouvant pour les joueurs. Les joueurs sont même encouragés à jouer de
 manière offensive, puisque le jeu considéré « passif » est sanctionné. L’endurance et la force sont donc
 des qualités primordiales pour un joueur de handball, cependant ce sport est également une affaire de
 tactique, de travail d’équipe et de polyvalence, tous les joueurs alternant les phases d’attaque puis de 
 défense.</br>
 Toutefois, dans la pratique du handball à haut-niveau, certains joueurs s’affirment comme des spécialistes
 de la défense et l’on peut donc observer des changements de joueurs (très) rapides à chaque phase de jeu
 afin de renforcer la défense et reposer certains joueurs en vue de la phase offensive.</br>
 Le handball est également un sport spectaculaire et technique puisque pour pouvoir marquer, les joueurs
 doivent tirer en dehors de la zone du gardien, un demi-cercle de 6m de rayon, et sont pour cela en
 extension dans les airs pour la majorité de leurs tirs et donnent ainsi l’impression de s’envoler.
 Le tir est également impressionnant de technicité, tant les joueurs savent manipuler le ballon et
 parfois donner à la balle des trajectoires incroyables. C’est en particulier vrai pour les joueurs
 évoluant en position d’ailier, dans les coins du terrain, qui ont un angle de tir très restreint et 
 présentent donc des qualités de poignet exceptionnelles. 
 <h3>Son pictogramme</h3>
<img src="Image/handball.webp"/>

</body>
</html>"""

print(html)