# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>L'escrime</h1>
<h3>Son histoire</h3>
<p>L’escrime est un sport olympique historique, qui faisait déjà partie du programme olympique aux Jeux 
d’Athènes en 1896, pour ne plus jamais le quitter. Les femmes entrent sur la piste à l’occasion des Jeux
 de 1924, à Paris. Aujourd’hui, hommes et femmes s’affrontent dans des épreuves individuelles et par
 équipes dans lesquelles sont utilisées trois types d’armes : le fleuret, l’épée et le sabre. Le fleuret 
 est d’abord la seule arme utilisée par les femmes jusqu’aux Jeux de 1996 à Atlanta, qui voient
 l’introduction de l’épée féminine. Le sabre féminin, quant à lui, figure pour la première fois au
 programme des Jeux de 2004 à Athènes.</br>
 Parmi les figures qui ont marqué ce sport, l’Italien Nedo Nadi est le seul escrimeur à avoir remporté 
 une médaille avec chacune des trois armes en une seule édition des Jeux. En 1912, à l’âge de 18 ans, il
 gagne au fleuret. Puis, après avoir été décoré par son pays pour acte de bravoure lors de la Première
 Guerre mondiale, il remporte cinq médailles d’or à Anvers en 1920, record historique et inégalé : en
 individuel au fleuret et au sabre, par équipes au fleuret, à l’épée et au sabre.
 </p>
 <h3>Ces règles et informations</h3>
 <pEn escrime, deux concurrents se font face, une arme à la main, et doivent toucher leur adversaire de
 leur arme sur une zone donnée. Chaque arme dispose de ses spécificités. Le sabre permet de toucher son 
 adversaire avec toutes les parties de la lame (pointe, tranchant et dos). La surface valable comprend le
 haut du corps à partir de la taille, incluant les bras et la tête. L’épée et le fleuret ne permettent de
 marquer des points qu’avec la pointe, mais sur l’ensemble du corps (incluant le masque et les chaussures)
 pour l’épée, et uniquement sur le tronc (buste, épaules et cou) pour le fleuret. L’escrimeur qui atteint
 quinze points, ou qui en a le plus à la fin du temps réglementaire remporte le combat. Par équipe, c’est 
 la première équipe à 45 points, ou qui en a le plus à la fin du temps réglementaire, qui l’emporte.
 <h3>Son pictogramme</h3>
<img src="Image/escrime.webp"/>

</body>
</html>"""

print(html)