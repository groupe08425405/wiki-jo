# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>Le volleyball</h1>
<h3>Son histoire</h3>
<p>C’est à Tokyo, lors des Jeux de 1964, que le volleyball fait son arrivée au programme olympique. 
Initialement, les compétitions féminine et masculine se déroulaient sous la forme d’un championnat où
 toutes les équipes se rencontraient. Finalement, huit ans plus tard, la compétition prend la forme d’un
 tournoi classique avec une phase de poule suivie d’un tour final avec quarts de finale, demi-finale puis 
 finale pour déterminer les médaillés.</br>
 Le palmarès olympique a été successivement dominé par l’Union Soviétique et Cuba dans les années 1980, 
 l’Italie et la Chine dans les années 1990, et enfin le Brésil dans les années 2000. Au tableau des
 médailles, l’Union Soviétique, le Brésil et les Etats-Unis restent en tête, et sont les trois seules
 nations à cumuler au moins dix médailles.
 </p>
 <h3>Ces règles et informations</h3>
 <p>Le volleyball oppose des équipes de six joueurs qui sont départagées au meilleur des cinq sets,
 sur un terrain de dix-huit mètres de long sur neuf mètres de large. Chaque set se joue en 25 points
 gagnants, et si les deux équipes remportent deux sets chacune, le cinquième se joue cette fois en 15
 points.</br>
 Au volleyball, le ballon peut atteindre une vitesse de 130km/h, notamment sur les services sautés et les
 smashs spectaculaire qui demandent une grande puissance, mais aussi des réflexes très développés pour 
 pouvoir les rattraper.
 <h3>Son pictogramme</h3>
<img src="Image/volleyball.webp"/>

</body>
</html>"""

print(html)