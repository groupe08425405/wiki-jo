# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>Le mountain bike</h1>
<h3>Son histoire</h3>
<p>Le Mountain Bike entre aux Jeux un siècle après ses aînés du cyclisme sur piste et sur route, 
à Atlanta en 1996, avec une course individuelle féminine et masculine. Dans sa courte histoire olympique,
 le Moutain Bike est dominé par la France et la Suisse qui terminent régulièrement sur le podium, se
 partageant 16 des 42 médailles attribuées dans l’épreuve.
 </p>
 <h3>Ces règles et informations</h3>
 <p>Le Mountain Bike dispose de deux épreuves, une féminine et une masculine, appelées cross-country. 
 Il s’agit d’une course en ligne de plusieurs tours autour d’une boucle, dont le terrain montagneux et 
 accidenté éprouve les qualités techniques, d’endurance et de résistance des athlètes.</br>
 Les courses se déroulent en départ groupé, et sont en général composées de plusieurs boucles pour 
 atteindre une distance de quelques dizaines de kilomètres, qui sera parcourue par les coureurs en 1h20
 à 1h40 de course intense et riche en rebondissements.
 <h3>Son pictogramme</h3>
<img src="Image/mountain_bike.webp"/>

</body>
</html>"""

print(html)