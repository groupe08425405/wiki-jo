# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>La gymnastique artistique</h1>
<h3>Son histoire</h3>
<p>La gymnastique fait partie des sports historiques des Jeux de l’ère moderne. Elle a été au programme 
olympique de chacune des éditions organisées depuis les Jeux de 1896, à Athènes, où seuls les hommes
 étaient autorisés à participer. Les épreuves féminines de gymnastique artistique seront incorporées
 au programme olympique en 1928, lors des Jeux Olympiques d’Amsterdam. Le programme de la gymnastique
 a beaucoup évolué au cours de la première moitié du 20e siècle, mais n’a ensuite plus bougé depuis 1960 
 pour les femmes, et 1936 pour les hommes.</br>
 Dans les années 1960 et 1970, le Japon dominait le palmarès olympique, pour ensuite marquer le pas 
 devant l’Union Soviétique et l’Allemagne de l’Est. Aujourd’hui, les pays qui dominent les débats en
 gymnastique artistique sont le Japon, les Etats-Unis, la Russie et la Chine.
 </p>
 <h3>Ces règles et informations</h3>
 <p>
La gymnastique artistique comporte des compétitions individuelles par agrès, ainsi que des concours
 individuels et par équipe sur l’ensemble des agrès. Les différents agrès sollicitent chacun des qualités
 spécifiques, et sont : les exercices au sol, le cheval d’arçons, les anneaux, le saut de cheval, les 
 barres parallèles et la barre fixe pour les hommes, ainsi que le saut de cheval, les barres asymétriques,
 la poutre et les exercices au sol pour les femmes. Dans ces différents exercices, spectaculaires,
 les figures et enchaînement demandent force, agilité, coordination et vitesse aux athlètes.</br>
 Jusqu’en 2004, les notations s’effectuaient sur un maximum de 10 points, mais à partir de 2005 le mode 
 de notation change pour une combinaison entre une note « D » (difficulté et contenu de l’exercice) et
 une note « E » d’exécution, afin de permettre une plus grande différenciation entre les notations des
 athlètes. La réflexion autour d’un changement de notation a débuté lors des Jeux de 1976, où une gymnaste
 roumaine, Nadia Comaneci, obtenait pour la première fois de l’histoire un score parfait de 10.
 <h3>Son pictogramme</h3>
<img src="Image/gymnastique_artistique.webp"/>

</body>
</html>"""

print(html)