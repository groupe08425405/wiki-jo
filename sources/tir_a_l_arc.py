# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>Le tir à l'arc</h1>
<h3>Son histoire</h3>
<p>La présence du tir à l’arc aux Jeux Olympiques s’étale sur deux périodes. Il apparaît très tôt au 
programme olympique, puisque dès 1900 une compétition est organisée aux cours des Jeux de Paris. Le tir
 à l’arc est disputé ensuite en 1904, 1908 et 1920, puis est sorti du programme pendant plus de 50 ans,
 jusqu’en 1972.</br>
 Aux Jeux de Munich, la discipline fait son grand retour au programme olympique, pour ne plus le quitter
 jusqu’à aujourd’hui. La Corée en domine largement le palmarès olympique depuis son retour au programme 
 en 1972, puisqu’elle a récolté plus de la moitié des médailles d’or en jeu (27 sur 45). 
 </p>
 <h3>Ces règles et informations</h3>
 <p>Le tir à l’arc se pratique sur une cible de 122cm de diamètre, positionnée à 70 mètres des tireurs,
 qui doivent placer leurs flèches le plus près possible du centre pour arriver à vaincre leurs adversaires.
  Le tir à l’arc comporte cinq épreuves, deux compétitions individuelles féminine et masculine, deux
  compétitions par équipe encore une fois féminine et masculine, et enfin pour la première fois au 
  programme aux Jeux de Tokyo 2020, une épreuve mixte.</br>
  Le tir à l’arc demande une concentration et une adresse à toute épreuve pour pouvoir dominer ses nerfs, 
  chaque erreur pouvant être fatale, notamment lors des duels qui composent les phases finales des épreuves
  individuelles. A l’issue du tour de qualification, qui voit les athlètes tirer 72 fois chacun, 
  les archers s’affrontent pour la compétition finale dans un système à élimination directe qui les
  emmène jusqu’en finale, en fonction de leur classement lors du tour de qualification
  (le premier affronte le 64e, le deuxième le 63e, etc…).
 <h3>Son pictogramme</h3>
<img src="Image/tir_a_l_arc.webp"/>

</body>
</html>"""

print(html)