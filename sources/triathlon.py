# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>Le triathlon</h1>
<h3>Son histoire</h3>
<p>Après la création en 1991 d’un circuit de Coupe du monde, comptant onze compétitions disputées dans huit
 pays différents, le sport se démocratise rapidement, si bien que le triathlon entre au programme olympique 
 aux Jeux de Sydney, en 2000. La discipline compte aujourd’hui 167 fédérations nationales à travers le monde . 
 Ce sport étant relativement jeune, aucun pays ne se démarque réellement au palmarès olympique, puisque
 les 39 médailles attribuées en six éditions ont été réparties entre 16 pays, dont aucun ne cumule plus de
 trois médailles d’or (la Grande-Bretagne).
 </p>
 <h3>Ces règles et informations</h3>
 <p>Le format du triathlon, retenu pour les épreuves olympiques féminines et masculines, fait enchaîner aux
athlètes 1500m de natation, 40km à vélo, et pour finir 10km de course à pied. Il n’y a pas de
 qualification mais une course unique à l’issue de laquelle l’athlète le plus complet est sacré vainqueur.</br>
 Pour les Jeux Olympiques de Tokyo 2020, aux deux épreuves individuelles masculine et féminine, a été 
 ajouté pour la première fois un relais mixte. Le relais se dispute dans un format court, rendant la 
 course nerveuse et imprévisible, et voit s’affronter des équipes de deux hommes et deux femmes.
 <h3>Son pictogramme</h3>
<img src="Image/triathlon.webp"/>

</body>
</html>"""

print(html)