# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>La natation</h1>
<h3>Son histoire</h3>
<p>La natation est une discipline historique des Jeux Olympiques de l’ère moderne. Si les premières courses
 olympiques se déroulaient en environnement naturel, dès les Jeux de Londres en 1908, les épreuves ont pris 
 place dans une piscine, ce qui a donné lieu à la création de la Fédération Internationale de Natation 
 (FINA). La nage libre et la brasse sont les seules épreuves présentes aux Jeux d’Athènes en 1896, le dos 
 est ensuite ajouté en 1904, puis le papillon apparaît en 1956 aux Jeux de Melbourne.</br>
 Les femmes font leur entrée au programme olympique en 1912, en participants à deux épreuves seulement.
 Leur nombre d’épreuve est aujourd’hui le même que celui des hommes, puisque les programmes masculin et
 féminin sont identiques depuis les Jeux de Tokyo en 2021. Le palmarès olympique de la natation est
 largement dominé par les Etats-Unis, qui détiennent plus de 250 médailles d’or.
 </p>
 <h3>Ces règles et informations</h3>
 <p>Aux Jeux Olympiques, les épreuves de natation se déroulent dans un bassin de 50m de long. Il existe 
 quatre types de nage en natation, qui sont courues soit en épreuves individuelles, soit en relais :
     la brasse, le papillon, le dos et la nage libre, toujours exécutée avec la technique du crawl. 
     Un cinquième type de course, la course de quatre nages, regroupe les quatre précédentes et voit 
     les nageurs enchaîner les quatre types de nages. Les distances varient également, et ne demandent 
     pas les mêmes qualités pour une course de 50m que pour un 1500m. L’explosivité tout comme l’endurance,
     la puissance et la technique sont des qualités indispensables pour les nageurs.
 <h3>Son pictogramme</h3>
<img src="Image/natation.webp"/>

</body>
</html>"""

print(html)