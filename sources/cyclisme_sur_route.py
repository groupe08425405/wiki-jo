# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>Le cyclisme sur route</h1>
<h3>Son histoire</h3>
<p>Le cyclisme sur route fait partie des disciplines historiques des Jeux Olympiques, puisque ce sport
 faisait déjà partie du programme des sports lors des Jeux de la Ière Olympiade moderne en 1896 à Athènes.
 Absent trois éditions de 1900 à 1908, le cyclisme sur route n’a plus manqué une seule édition des Jeux 
 Olympiques, et les femmes sont entrées au programme en 1984, alors que l’épreuve de contre-la-montre a
 été intégrée en 1996.</br>
 Jusqu’en 1996, les coureurs professionnels n’étaient pas autorisés à participer aux Jeux Olympiques.
 Depuis les Jeux d’Atlanta, les membres du peloton professionnel sont conviés, redonnant du prestige à
 des épreuves olympiques désormais trustées par les meilleurs coureurs mondiaux.
 </p>
 <h3>Ces règles et informations</h3>
 <p>Le cyclisme sur route se court en extérieur, et comporte deux types d’épreuves aux Jeux Olympiques ;
 une course en ligne, ainsi qu’une course contre-la-montre individuelle.</br>
 La course en ligne se fait en départ groupé, et comporte une grande part de tactique et d’endurance ; 
 bien souvent, les courses de plus de 120 kilomètres pour les femmes et plus de 200 kilomètres pour les
 hommes se jouent sur un sprint final de quelques centaines de mètre, ce qui exige des coureurs un
 placement irréprochable et une capacité à s’économiser pendant toute la première partie de la course.</br>
 Le contre-la-montre est un effort solitaire, puisque les coureurs partent cette fois chacun leur tour
 à intervalle régulier, et non pas en peloton. L’effort est également plus court, puisqu’un
 contre-la-montre se déroule sur une distance dépassant rarement les 50 kilomètres. La régularité, 
 la concentration, l’économie des efforts grâce à une position aérodynamique ainsi que la puissance
 développée par le coureur font la différence dans cette épreuve.</bt>
<img src="Image/cyclisme_sur_route.webp"/>

</body>
</html>"""

print(html)