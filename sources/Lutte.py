# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>La lutte</h1>
<h3>Son histoire</h3>
<p>Du fait de son origine, la lutte gréco-romaine fait partie des épreuves reines dès la rénovation des 
Jeux Olympiques, à Athènes en 1896. Dès 1908, elle intègre de manière permanente le programme olympique.
 La lutte libre fait son arrivée plus tard, faisant une apparition aux Jeux de 1904 à Saint Louis, puis
 s’inscrivant définitivement au programme olympique à partir de 1920, à l’occasion des Jeux Olympiques
 d’Anvers. A partir des Jeux d’Athènes en 2004, des épreuves féminines entrent au programme olympique, 
 mais uniquement en lutte libre.</br>
 L’ex-URSS mène le palmarès de la lutte olympique, avec 116 médailles dont 62 titres. Elle est suivie
 par plusieurs autres nations fortes à l’image des Etats-Unis (142 médailles dont 57 titres), de l’Iran,
 de la Russie, de la Turquie ou encore du Japon, première nation féminine avec 15 médailles d’or sur 24 
 possibles.
 </p>
 <h3>Ces règles et informations</h3>
 <p>Les deux styles de lutte ont chacun leurs spécificités. En lutte gréco-romaine, il n’est permis
 d’attaquer qu’en utilisant les bras et la partie supérieure du corps. En lutte libre, les possibilités
 d’attaque sont plus variées puisqu’il est possible d’utiliser également les jambes, et de ceinturer le 
 haut comme le bas du corps.</br>
 Cependant le but des deux variantes reste identique ; disputés sur une surface de combat de forme 
 circulaire, les combats durent deux fois trois minutes. L’objectif des athlètes est, sans saisir le 
 maillot de l’adversaire et à mains nues, de coller les omoplates de l’adversaire au sol
 (c’est « le tombé »), ou à défaut de totaliser le plus de points à la fin du temps règlementaire, par la 
 réalisation de techniques cotées de renversement debout et de retournement au sol.
 <h3>Son pictogramme</h3>
<img src="Image/lutte.webp"/>

</body>
</html>"""

print(html)