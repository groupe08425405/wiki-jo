# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body class="body">
<h1>Sports équestre</h1>
<h3>Son histoire</h3>
<p>Les sports équestres sont présents pour la première fois aux Jeux Olympiques de 1900, et entrent 
définitivement au programme olympique lors des Jeux de Stockholm, en 1912. A partir de 1952, lors des
 Jeux d’Helsinki, les femmes sont autorisées à prendre part à l’épreuve de dressage, désormais mixte.
 En 1964, les femmes prennent part à toutes les épreuves d’équitation, faisant de l’équitation le seul
 sport réellement entièrement mixte aux Jeux Olympiques. Des médailles individuelles et par équipes sont 
 décernées.
 </p>
 <h3>Ces règles et informations</h3>
 <p>Les sports équestres comptent trois disciplines présentes aux Jeux, où les hommes et les femmes
 concourent dans la même catégorie mixte.</br>
 Le saut d’obstacle consiste en un parcours chronométré où les cavaliers doivent également faire tomber
 aussi peu de barres d’obstacles que possible, chaque barre renversée entraînant des pénalités. L’agilité, 
 la technique et le contrôle du couple cheval-cavalier sont sollicitées.</br>
 Le dressage est l’expression la plus poussée de l’entraînement d’un cheval.  Le couple cheval/cavalier
 est mis en scène artistiquement dans une série de figures exécutées au cours d’une reprise. Un jury juge
 l’aisance et la fluidité de l’évolution du couple dans la carrière.</br>
 Une troisième épreuve, le concours complet, s’apparente à une sorte de triathlon équestre. Il combine 
 deux épreuves telles que celles que nous venons de décrire (le saut d’obstacle et le dressage) avec une
 troisième, le cross-country. Ce dernier consiste en un long parcours mêlant des obstacles naturels ou
 fixes, qui sollicitent cette fois l’endurance et l’expérience des compétiteurs.  
 Le couple cheval-cavalier le plus polyvalent l’emporte à l’issue des trois épreuves.
 <h3>Ses pictogrammes</h3>
 <div class="cheval">
<img class="equestre" src="Image/dressage.webp"/>
</div>
 <div class="cheval">
<img class="equestre" src="Image/sauts_d_obstacles.webp"/>
</div>
 <div class="cheval">
<img class="equestre" src="Image/concours_complet.webp"/>
</div>



</body>
</html>"""

print(html)