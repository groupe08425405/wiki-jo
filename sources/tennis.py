# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>Le tennis</h1>
<h3>Son histoire</h3>
<p>Historiquement, la présence du tennis aux Jeux Olympiques s’inscrit en pointillés. Dès la première 
édition moderne des Jeux Olympiques, en 1896 à Athènes, le tennis est présent. Cependant il quitte le
 programme olympique à partir de 1924, à cause de problématiques autour de la professionnalisation du
 sport. Malgré un retour en sport de démonstration à Mexico en 1968, la petite balle jaune ne revient
 définitivement au programme olympique qu’en 1988, à Séoul. </br>
 Depuis, les meilleurs mondiaux viennent à chaque édition disputer les compétitions olympiques. Lors des
 dernières éditions, des poids lourds de l’histoire de ce sport se sont illustrés, comme Rafael Nadal,
 champion olympique en simple en 2008 puis en double en 2016, Stan Wawrinka et Roger Federer, champions 
 olympiques de double en 2008, ou encore les sœurs Williams, Serena et Vénus, qui comptent à elles deux 
 huit médailles d’or olympiques.</p>

 <h3>Ces règles et informations</h3>
 <p> Aux Jeux Olympique, le tennis dispose d'épreuves masculines et féminines de simple et de double,
  ainsi que d'une épreuve de double mixte. Les épreuves individuelles hommes et femmes se jouent au meilleur des tois sets.
  Le tie-break est instauré dans la manche décisive pour discipline, à l'exeption du double mixte
  où; en cas d'égalité à un set partout, un super tie-break est joué pour départager les joueurs.
 <h3>Son pictogramme</h3>
<img src="Image/tennis.webp"/>

</body>
</html>"""

print(html)