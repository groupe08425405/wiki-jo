# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>Escalade sportive</h1>
<h3>Son histoire</h3>
<p>L’escalade sportive a fait ses premiers pas dans le monde olympique aux Jeux Olympiques de la Jeunesse,
 à Buenos Aires en 2018. Le public, pourtant peu averti, a été impressionné par le spectacle et le suspens
 offerts par ce sport spectaculaire. Après ce succès populaire chez les jeunes, l’escalade sportive a fait
 ses grands débuts aux Jeux Olympiques de Tokyo, en 2021, après avoir été incorporée au programme olympique
 en tant que « nouveau sport ». La diversité de ses épreuves, son côté visuel, esthétique et créateur 
 d’émotions en font un sport très apprécié et pratiqué par la jeunesse, qui peut s’insérer dans des 
 environnements très différents, urbains ou naturels. L’escalade sportive fera également partie des
 quatre sports additionnels de Paris 2024, en compagnie du surf, du breaking et du skateboard, et sera 
 aussi au programme à Los Angeles, en 2028. 
 </p>
 <h3>Ces règles et informations</h3>
 <p>Au programme olympique de l’escalade sportive, les épreuves regroupent trois disciplines, le bloc, 
 la vitesse et la difficulté. Le « bloc » consiste à escalader des structures de 4,5m de hauteur, sans
 corde mais avec des tapis de réception, dans un temps contraint et avec le moins de tentatives possibles.</br>
 L’épreuve de vitesse consiste en une spectaculaire course contre la montre en duels éliminatoires en un
 contre un, mêlant précision et explosivité. Les meilleurs athlètes grimpent un mur incliné à 5 degrés de
 15 mètres en moins de 6 secondes pour les hommes et moins de 7 secondes pour les femmes.</br>
 L’épreuve de difficulté, enfin, demande aux athlètes de grimper le plus haut possible un mur de 15m, 
 en six minutes, et sans connaître la voie en avance. Les parcours de l’escalade de « difficulté » sont
 de plus en plus complexes et exigeants au fil de la compétition, sollicitant toutes les capacités 
 physiques et mentales des athlètes.</br>
 A Tokyo, chaque concurrent concourait dans les trois disciplines, et le classement final reflétait les
 résultats combinés des trois épreuves. L’athlète ayant le score le plus faible devenait le premier
 champion olympique de l’histoire de l’escalade sportive.</br>
 A Paris 2024, deux épreuves d’escalade distinctes sacreront deux champions et deux championnes olympiques
 ; une première combinant bloc et difficulté, et une deuxième uniquement de vitesse.
 <h3>Son pictogramme</h3>
<img src="Image/escalade.webp"/>

</body>
</html>"""

print(html)