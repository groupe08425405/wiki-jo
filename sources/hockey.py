# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>Le hockey</h1>
<h3>Son histoire</h3>
<p>Le hockey apparait pour la première fois aux Jeux en 1908, à Londres. Il fait son entrée définitive
 au programme olympique en 1928, à Amsterdam. Les femmes font leur entrée au programme en 1980, aux Jeux
 de Moscou.</br>
 Du fait de son origine moderne anglo-saxonne, le hockey est logiquement dominé par des pays membres ou
 anciens membres de l’Empire Britannique tels que l’Inde, le Pakistan, l’Australie, la Nouvelle-Zélande,
 l’Afrique du Sud et bien évidemment la Grande-Bretagne. L’équipe masculine d’Inde est celle qui appose
 la plus forte domination sur le palmarès olympique. L’Inde est en effet huit fois médaillée d’or, avec 
 six titres consécutifs dans le tournoi masculin entre 1928 et 1956. Sur cette période, elle enchaîne
 notamment trente victoires sans la moindre défaite avec 197 buts inscrits et seulement huit encaissés.
 Néanmoins, d’autres nations – comme l’Argentine, les Pays-Bas, l’Allemagne, la Belgique, l’Espagne et 
 la France – se sont hissées aux premières places du classement mondial, rendant ce sport véritablement 
 planétaire.
 </p>
 <h3>Ces règles et informations</h3>
 <p>Vitesse, technicité, fun !  Voilà les principales caractéristiques du hockey, sport exaltant et ô 
 combien dynamique, auxquelles ils convient d’ajouter l’endurance et une grande coordination. Un match de 
 hockey se joue à onze contre onze et dure 60 minutes. Il se compose de 4 périodes de 15 minutes. Sur le
 terrain, chaque équipe dispose de dix joueurs de champ en position d’attaquants, de milieux de terrain
 et de défenseurs ainsi que d’un gardien de but. Chaque joueur peut être remplacé à tout moment et aussi
 souvent que voulu. Une équipe peut faire le choix de jouer sans gardien (temporairement, en principe). 
 Dans les matches à élimination directe, une séance de shootouts départage les équipes à égalité à l’issue
 du temps réglementaire.</br>
 Si lors des débuts de ce sport, l’herbe était naturelle, elle ralentissait le jeu. Le choix d’un gazon 
 synthétique a été fait pour rendre le jeu plus rapide, et plus moderne. Le terrain est également mouillé,
 ce qui accélère encore plus le mouvement de la balle.</br>
 Les athlètes utilisent une crosse en forme de crochet, avec laquelle ils conduisent, contrôlent et 
 frappent une balle dure. Seul le côté plat du stick peut être utilisé. A l’exception du gardien, les 
 joueurs ne sont pas autorisés à toucher la balle avec leurs mains ou leurs pieds.</br>
 Une faute peut conduire l’arbitre à infliger un carton vert (2 minutes de suspension), jaune
 (5 minutes de suspension) ou rouge (exclusion du joueur). Un terrain de hockey mesure 91,4m de long et
 55m de large. Les buts, situés à chaque extrémité, sont entourés par une zone de tir en forme de D. Les
 buts ne peuvent être marqués que depuis l’intérieur de la zone de tir de l’adversaire.
 <h3>Son pictogramme</h3>
<img src="Image/hockey.webp"/>

</body>
</html>"""

print(html)