# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>Skateboard</h1>
<h3>Son histoire</h3>
<p>Après avoir fait ses grands débuts sur la scène olympique aux Jeux Olympiques de la Jeunesse de Nanjing
 en 2014, le skateboard passe la vitesse supérieure et débarque aux Jeux Olympiques à Tokyo, en 2021, en 
 tant que « nouveau sport ». Le skateboard fait aussi partie des quatre sports additionnels de Paris 2024,
 et enflammera a n’en pas douter la place de la Concorde, transformée en arène sportive pour l’occasion.
 </p>
 <h3>Ces règles et informations</h3>
 <p>L’élite du skateboard mondial s’affrontera aux Jeux Olympiques dans deux des disciplines les plus
 populaires et spectaculaires du skateboard, le Park et le Street, où les athlètes devront réaliser les
 plus beaux tricks, répondant à des critères de technique, de vitesse ou encore d’amplitude de leurs 
 figures.</br>
 Les épreuves seront composées de deux manches, une manche préliminaire de qualification, puis une manche
 finale.</br>
 Le Park prend place dans un terrain varié, combinant bowls et de nombreuses courbes utilisées par le
 s athlètes afin de prendre de la vitesse ou de s’envoler pour réaliser des enchaînements de figures.</br>
 Dans cette discipline, la hauteur et la vitesse de réalisation des figures pendant les sauts sont
 évaluées, ainsi que la capacité des skaters à utiliser l’ensemble de la surface et ses aspérités. 
 Les athlètes sont notés sur trois passages de quarante-cinq secondes. Le meilleur des passages est
 retenu pour définir le classement final.</br>
 Le Street, lui, se déroule dans un décor reproduisant les éléments d’une rue, comme les escaliers, 
 les rails… à l’image du terrain de jeu originel des skateurs. Les athlètes doivent enchaîner les figures,
 et sont aussi jugés sur leur capacité à maîtriser leur planche au cours des deux runs de quarante-cinq
 secondes et cinq figures réalisées.
 <h3>Son pictogramme</h3>
<img src="Image/skateboard.webp"/>

</body>
</html>"""

print(html)