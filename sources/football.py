# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>Le football</h1>
<h3>Son histoire</h3>
<p>Le football est arrivé très tôt au programme olympique, dès les Jeux de Paris en 1900. Il ne l’a ensuite
 pas quitté. Le football féminin fait son entrée au programme olympique aux Jeux d’Atlanta, en 1996. Les
 Etats-Unis ont été sur la première marche du podium de nombreuses fois, à Atlanta en 1996, Athènes 2004,
 Pékin 2008 et Londres 2012. En 2016 l’Allemagne s’est imposée, alors qu’à Tokyo ce sont les canadiennes 
 qui se sont imposées.</br>
 Chez les hommes, l’Europe a dominé le palmarès olympique du football jusqu’aux Jeux de 1992 à Barcelone,
 où l’Espagne devenait la dernière équipe européenne à remporter le titre olympique. Depuis Atlanta en 1996, 
 les pays africains et d’Amérique Latine ont pris le relais en remportant chaque titre, notamment le
 Brésil qui s’est imposé en 2016 et 2021. Beaucoup de grands noms du football ont inscrit les Jeux
 Olympiques à leur palmarès, comme Ferenc Puskás en 1952, Lev Yachine en 1956, Samuel Eto’o en 2000,
 Lionel Messi en 2008, Neymar en 2012 et 2016, Marta en 2004, 2008, 2012, 2016 et 2020, ou encore Alex
 Morgan en 2012, 2016 et 2020.
 <h3>Ces règles et informations</h3>
 <p>Les matchs des deux tournois se disputent sur un terrain de football en gazon, à 11 contre 11,
 pendant deux mi-temps de 45 minutes. La compétition féminine ne diffère pas de celles organisées par la
 FIFA, opposant les équipes senior des pays qualifiés. Une différence apparaît dans les règles du tournoi 
 masculin quant à la composition des effectifs. Chaque équipe doit être entièrement composée de joueurs
 nés le ou après le 1er janvier 2001 (soit âgés de 23 ans au moment de Paris 2024). Cependant, trois
 footballeurs nés avant cette date peuvent être inclus dans chaque effectif composé de 18 joueurs.
 <h3>Son pictogramme</h3>
<img src="Image/football.webp"/>

</body>
</html>"""

print(html)