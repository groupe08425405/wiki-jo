# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>Le breaking</h1>
<h3>Son histoire</h3>
<p>Le breaking a fait son apparition au programme des Jeux Olympiques de la Jeunesse de Buenos Aires, 
en 2018. Après le grand succès rencontré par la discipline lors de cette manifestation, c’est à l’occasion
 des Jeux de Paris 2024 que le breaking entrera encore un peu plus dans le monde de l’olympisme. 
 Il s’inscrira cette fois au programme des Jeux Olympiques, en tant que sport additionnel en compagnie
 du surf, du skateboard et de l’escalade. 
 </p>
 <h3>Ces règles et informations</h3>
 <p>Lors des Jeux de Paris 2024, la compétition de breaking sera composée de deux épreuves, une masculine
 et une féminine, qui verront respectivement 16 B-Boys et 16 B-Girls s’affronter dans des battles
 spectaculaires (1-vs-1). Les athlètes enchaîneront les « powermoves » comme les coupoles, les six-step
 ou encore les freezes en s’adaptant et en improvisant sur le son du DJ pour s’adjuger les votes des juges,
 et remporter le premier titre olympique de breaking.
 <h3>Son pictogramme</h3>
<img src="Image/breaking.webp"/>

</body>
</html>"""

print(html)