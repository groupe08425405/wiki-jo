# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>Voile</h1>
<h3>Son histoire</h3>
<p>Dès les premiers Jeux Olympiques de l’ère moderne, la voile est au programme olympique. Seulement,
 les mauvaises conditions qui règnent le 1er avril 1896 à Athènes contraignent les organisateurs à annuler
 les épreuves. Les premières épreuves tenues seront donc celles des Jeux Olympiques de 1900, à Paris.
 Depuis, les catégories autorisées dans la compétition évoluent en permanence. Les différentes épreuves
 sont organisées en fonction des catégories monotypes des bateaux, c’est-à-dire de leur taille et leur
 poids.
 </p>
 <h3>Ces règles et informations</h3>
 <p>La voile consiste à se déplacer avec un bateau, seulement à l’aide de la force du vent. Les conditions,
 toujours changeantes, requièrent beaucoup de technique et d’expérience de la part des athlètes pour être
 maîtrisées. Les règles de la fédération internationale de voile, World Sailing, sont appliquées pour les
 épreuves olympiques. Les compétitions sont composées de régates en flottes, qui opposent des bateaux
 identiques sur un même parcours varié. </br>
 Dix épreuves ont eu lieu lors des Jeux de Tokyo, de la planche à voile à l’épreuve mixte du Nacra 17 en 
 passant par les 49ers ou encore les 470. Les différentes disciplines de la voile sont en constante 
 évolution, et les voiliers utilisés sont conçus pour être de plus en plus petits et légers, requérant
 toujours plus de capacités athlétiques et techniques de la part des marins. Deux épreuves jusqu’ici 
 jamais vues aux Jeux Olympiques verront le jour à Paris en 2024 : la planche à voile IQ foil et le 
 formula kite.
 <h3>Son pictogramme</h3>
<img src="Image/voile.webp"/>

</body>
</html>"""

print(html)