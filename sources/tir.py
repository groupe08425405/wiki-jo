# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>Le tir</h1>
<h3>Son histoire</h3>
<p>Le tir est un sport olympique historique, présent dès les premiers Jeux Olympiques modernes en 1896,
 à Athènes, à l’exception des Jeux de 1904 et de 1928. Le nombre d’épreuves, qui a beaucoup évolué au fil
 des éditions, est passé de cinq en 1896, à quinze aujourd’hui. Les Etats-Unis sont le pays qui détient de
 loi le plus de médailles olympiques, ensuite viennent la Chine et la Russie (sous le nom de l’URSS).
 L’Italie fait également très bonne figure, remportant notamment à Rio en 2016 trois médailles d’or et
 trois médailles d’argent, sous l’impulsion des deux titres olympiques de Niccolo Campriani.
 </p>
 <h3>Ces règles et informations</h3>
 <p>Il existe trois différents types d'épreuves de tir aux Jeux Olympiques : à la carabine (rifle), 
 au pistolet et au fusil. Les épreuves de carabines et de pistolet prennent place dans des stands de tir,
 où les athlètes doivent atteindre des cibles de 10, 25 et 50 mètres de distance. Il y a aussi trois positions de tir : à genoux, où l'athlète se met à genoux
 et pose son coude sur son genou, couché et debout. Certaines épreuves comportent toutes ces positions.</br>
 Pour atteindre les cibles, les athlètes utilisent des méthodes de relaxation qui leur permettent de
 réduire leur rythme cardiaque, pour atteindre la plus grande précision et se rapprocher le plus
 possible du centre de la cible, appelé la mouche. Les épreuves au fusil diffèrent dans le principe,
 puisqu’elles se déroulent en plein air. Les tireurs doivent atteindre des cibles projetées en l’air dans
 des angles et directions variables. Le tir est extrêmement exigeant pour la concentration, la prise de
 décision et les réflexes des athlètes.
 <h3>Son pictogramme</h3>
<img src="Image/tir.webp"/>

</body>
</html>"""

print(html)