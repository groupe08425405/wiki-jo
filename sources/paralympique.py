import cgi # Notre script va retourner du HTML 
print("Content-type: text/html; charset=UTF_8\n")
html = """<!DOCTYPE html> <html lang="fr"> 
<head> <meta charset="UTF_8"> 
<title> Ma page Web </title>
<link rel="stylesheet" href="style/styles.css"> 
</head> 
<body> 
<h1> Les jo paralympique de Paris 2024</h1> 
    <div class="jsp">
        <p> Les Jeux Paralympiques représentent une célébration remarquable de l'esprit humain, de l'athlétisme et de la résilience. Leur création remonte à l'après-Seconde Guerre mondiale, lorsqu'un neurologue britannique visionnaire, Sir Ludwig Guttmann, a organisé un événement sportif pour des vétérans handicapés dans un hôpital de Stoke Mandeville, en Angleterre, en 1948. Cet événement inaugural, qui comportait des épreuves de tir à l'arc, a posé les bases des Jeux Paralympiques modernes.</p>
        <img src="images/premierjeux.jpg" class="image-resize" />
    </div>
</br>
    <div class="jsp">
        <p>Depuis lors, les Jeux Paralympiques ont évolué pour devenir l'un des plus grands événements sportifs au monde, mettant en vedette des athlètes extraordinaires venus de tous les coins de la planète. Ils ont été officiellement associés aux Jeux Olympiques en 1960 à Rome, et depuis lors, ces deux événements sont organisés dans la même ville hôte, bénéficiant d'une attention mondiale massive.</p>
        <img src="images/logo.png" class="image-resize" />
    </div>
</br>
    <div class="jsp">
        <p>Les Jeux Paralympiques sont un festival de compétition, d'inspiration et d'inclusion. Les athlètes y participent dans diverses disciplines sportives, adaptées en fonction de leur handicap, telles que l'athlétisme, la natation, le basketball en fauteuil roulant, le ski alpin, le tennis en fauteuil roulant et bien d'autres. Ces compétitions démontrent la puissance de l'esprit humain, mettant en lumière le talent, la détermination et la persévérance des athlètes paralympiques.</p>
        <img src="images/determination.jpg" class="image-resize" />
    </div>
</br>
    <div class="jsp">
        <p>Le déroulement des Jeux Paralympiques suit généralement une structure similaire à celle des Jeux Olympiques, avec une cérémonie d'ouverture spectaculaire, où les athlètes défilent fièrement sous les couleurs de leur pays, suivie de jours d'intenses compétitions. Les épreuves se déroulent dans des installations sportives adaptées pour répondre aux besoins des athlètes handicapés, garantissant des conditions équitables et sécurisées pour tous les participants.</p>
        <img src="images/paris.jfif" class="image-resize" />
    </div>
</br>
    <div class="jsp">
        <p>Outre la compétition, les Jeux Paralympiques offrent une plateforme importante pour sensibiliser le public aux défis auxquels sont confrontés les personnes handicapées et pour promouvoir l'inclusion et l'égalité des chances dans la société. Ils inspirent également des millions de personnes à travers le monde en montrant que les obstacles peuvent être surmontés avec détermination, travail acharné et esprit sportif.</p>
        <img src="images/inspiration.jpg" class="image-resize" />
    </div>
</br>
    <div class="jsp">
        <p>En fin de compte, les Jeux Paralympiques sont bien plus qu'une simple compétition sportive. Ils incarnent les valeurs universelles de respect, d'unité et de solidarité, tout en célébrant la diversité et la force de la condition humaine. Ces jeux continueront à inspirer et à transformer des vies pour les générations à venir, en laissant un héritage durable d'inclusion et d'espoir pour un monde meilleur.</p>
        <img src="images/valeurs.png" class="image-resize" />
    </div>
</br>
    <div class="lien">    
        <a href="page_jeux_relais.py">Lien vers un Jeux </a>
    </div>
</body> 
</html>"""
print(html)