# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>Le cyclisme sur piste</h1>
<h3>Son histoire</h3>
<p>Discipline olympique historique et présente depuis les premiers Jeux de l’ère moderne, 
le cyclisme sur piste ne disparaît du programme olympique qu’à une brève reprise, aux Jeux Olympiques 
de Stockholm, en 1912. Les femmes ont dû cependant attendre les Jeux de Séoul 1988 pour entrer au
 programme olympique.</br>
 Au cours de ces plus de 120 ans d’histoire, les pays européens se sont taillé la part du lion, avec
 notamment la Grande-Bretagne, la France et les Pays-Bas ou encore l’Italie. Mais les autres pays ne sont
 pas en reste et progressent à chaque édition, à l’image de l’Australie qui remportait dix médailles dont
 six en or lors des championnats du monde 2019.
 </p>
 <h3>Ces règles et informations</h3>
 <p>Le cyclisme sur piste se court ainsi dans un vélodrome, dans différents formats d’épreuves de vitesse 
 individuelles et par équipes. Un vélodrome dispose d’une piste de 250m aux virages relevés, sur laquelle 
 les cyclistes évoluent avec une aisance impressionnante. Les vélos diffèrent principalement de ceux
 utilisés sur route par leur pignon fixe, et leur absence de freins.</br>
 Plusieurs types de courses se déroulent sur piste, chacune détient ses spécificités, certaines reposent
 plus sur la tactique ou la puissance que d’autre ; il y a donc une certaine différence entre un effort de 
 quelques tours en sprint individuel, et la course de 25km en groupe qui clôture l’omnium.
 <h3>Son pictogramme</h3>
<img src="Image/cyclisme_sur_piste.webp"/>

</body>
</html>"""

print(html)