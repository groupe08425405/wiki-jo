# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>Le tennis de table</h1>
<h3>Son histoire</h3>
<p>Le tennis de table arrive aux Jeux Olympiques en 1988, à Séoul, et inclut à ce moment-là les épreuves
 de simple et de double, masculin et féminin. En 2008, les épreuves de double sont remplacées par des
 épreuves par équipes, un programme enrichi pour les Jeux de Tokyo 2020 d’une nouvelle épreuve de double
 mixte, portant le nombre d’épreuves à cinq et permettant d’avoir le même nombre d’athlètes féminins et
 masculins participant aux épreuves de tennis de table aux Jeux Olympiques.</br>
 Le tennis de table a été dominé jusqu’au milieu du 20e siècle par les pays d’Europe centrale comme la
 Hongrie, la République Tchèque, l’Autriche et l’Allemagne. Mais depuis son arrivée aux Jeux Olympiques
 de Séoul en 1988, le tennis de table est largement dominé par la Chine, qui a remporté 32 des 37 
 médailles d’or mises en jeu.
 </p>
 <h3>Ces règles et informations</h3>
 <p>Le tennis de table se joue sur une table séparée en deux par un filet. Les joueurs s’échangent
 une balle ultra légère à l’aide de raquettes aux matériaux désormais complexes, dont les propriétés de
 vitesse et de rotation sont recherchées par les pongistes en fonction de leur style de jeu (offensif,
 défensif, mi-distance…).</br>
 Les matchs en simple sont joués en quatre manches gagnantes, soit sept manches au maximum.
 Chacune des manches est remportée par le premier joueur à onze points, avec une marge nette de deux points
 par rapport à son adversaire.</br>
 Les rencontres par équipes sont disputées en quatre matchs de simple ainsi qu’un match de double, tous
 disputés au meilleur des cinq manches. Des équipes de trois joueurs s’affrontent, et les matchs 
 s’achèvent dès qu’une équipe a remporté trois matchs. La spécificité des matchs de double est que
 les joueurs d’une équipe frappent la balle chacun leur tour. 
 <h3>Son pictogramme</h3>
<img src="Image/tennis_de_table.webp"/>

</body>
</html>"""

print(html)