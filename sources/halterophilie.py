# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>L'haltérophilie</h1>
<h3>Son histoire</h3>
<p>L’haltérophilie était présente dès les premiers Jeux Olympiques modernes d’Athènes, en 1896. Si
 pendant quelques éditions (1900, 1908 et 1912), elle quitte le programme olympique, l’haltérophilie
 revient ensuite au programme des Jeux d’Anvers en 1920, pour ne plus le quitter. Les femmes ont eu leurs
 épreuves à partir des Jeux Olympiques de Sydney, en 2000.</br>
 Au début du 20e siècle, les pays européens dominent l’haltérophilie, en particulier l’Allemagne,
 l’Autriche et la France. Les athlètes soviétiques trustent ensuite les podiums à partir des années 50,
 avant que la Chine, la Turquie, la Grèce et l’Iran n’arrivent sur le devant de la scène dans les années
 1990.  Côté féminin, la Chine domine depuis l’arrivée de la discipline chez les femmes. 
 </p>
 <h3>Ces règles et informations</h3>
 <p>Le programme d'haltérophilie aux Jeux a beaucoup évolué. Depuis les Jeux de Montréal 1976, deux
 techniques de lever composent les épreuves olympiques : l'arraché et l'épaulé-jeté.La technique de
 l'arraché consiste à soulever la barre depuis le sol jusqu'au-dessus de la tête en un seul mouvement.
 En ce qui concerne l'épaulé-jeté, le mouvemant se fait en deux étapes ; la barre est dans un premier temps montée 
 et posée sur l'vant des épaules, pour ensuite être projetée au-dessus de la tête. Ces 
 exercices ultra-exigeants requièrent une force physique hors-norme ainsi qu'un mental à toute épreuve.</br>
 Le concours se déroule aujourd’hui en combinant les résultats d’une épreuve d’arraché et d’une
 épreuve d’épaulé-jeté. Après trois essais dans chaque mouvement, le concurrent soulevant le plus au
 cumul de son meilleur essai dans chaque exercice est sacré champion olympique. Les hommes et les femmes
 s’affrontent dans cinq catégories de poids chacun.
 <h3>Son pictogramme</h3>
<img src="Image/halterophilie.webp"/>

</body>
</html>"""

print(html)