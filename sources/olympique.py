# -*- coding: utf-8 -*-
"""
Created on Tue Jan 23 14:38:19 2024

@author: lbelair
"""
import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body class="JO">
	<h1 class="titre_debut"> Les jeux Olympique</h1>
    <p> Voici tout les sports présent cette année au JO de Paris.  Cliquez sur celui de votre choix afin d'obtenir des infos.</p>
    <div></div>
    <div class="button-container-page1">
        <div>
       <form method="post" action="athletisme.py">
       <button type="submit" name="inscript">Athlétisme</button>
       </form>
       <form method="post" action="aviron.py">
       <button type="submit" name="inscript">Aviron</button>
       </form>
       <form method="post" action="badminton.py">
       <button type="submit" name="inscript">Badminton</button>
       </form>
       <form method="post" action="basketball.py">
       <button type="submit" name="inscript">Basketball</button>
       </form>
        </div>
        <div>
       <form method="post" action="basketball_3x3.py">
       <button type="submit" name="inscript">Basketball 3x3</button>
       </form>
       <form method="post" action="boxe.py">
       <button type="submit" name="inscript">Boxe</button>
       </form>
       <form method="post" action="canoe_sprint.py">
       <button type="submit" name="inscript">Canoë sprint</button>
       </form>
       <form method="post" action="canoe-kayak_slalom.py">
       <button type="submit" name="inscript">Canoë-kayak slalom</button>
       </form>
      </div>
        <div>
       <form method="post" action="cyclisme_sur_piste.py">
       <button type="submit" name="inscript">Cyclisme sur piste</button>
       </form>
       <form method="post" action="cyclisme_sur_route.py">
       <button type="submit" name="inscript">Cyclisme sur route</button>
       </form>
       <form method="post" action="BMX_freestyle.py">
       <button type="submit" name="inscript">BMX freestyle</button>
       </form>
       <form method="post" action="BMX_racing.py">
       <button type="submit" name="inscript">BMX racing</button>
       </form>
     </div>
        <div>
       <form method="post" action="Mountain_bike_(VTT).py">
       <button type="submit" name="inscript">Mountain bike (VTT)</button>
       </form>
       <form method="post" action="escrime.py">
       <button type="submit" name="inscript">Escrime</button>
       </form>
       <form method="post" action="football.py">
       <button type="submit" name="inscript">Football</button>
       </form>
       <form method="post" action="golf.py">
       <button type="submit" name="inscript">Golf</button>
       </form>
      </div>
        <div>
       <form method="post" action="gymnastique_artistique.py">
       <button type="submit" name="inscript">Gymnastique artistique</button>
       </form>
       <form method="post" action="gymnastique_rythmique.py">
       <button type="submit" name="inscript">Gymnastique rythmique</button>
       </form>
       <form method="post" action="trampoline.py">
       <button type="submit" name="inscript">Trampoline</button>
       </form>
       <form method="post" action="halterophilie.py">
       <button type="submit" name="inscript">Haltérophilie</button>
       </form>
      </div>
        <div>
       <form method="post" action="handball.py">
       <button type="submit" name="inscript">Handball</button>
       </form>
       <form method="post" action="hockey.py">
       <button type="submit" name="inscript">Hockey</button>
       </form>
       <form method="post" action="judo.py">
      <button type="submit" name="inscript">Judo</button>
       </form>
       <form method="post" action="lutte.py">
       <button type="submit" name="inscript">Lutte</button>
       </form>
      </div>
        <div>
       <form method="post" action="pentathlon_moderne.py">
       <button type="submit" name="inscript">Pentathlon moderne</button>
       </form>
       <form method="post" action="rugby.py">
       <button type="submit" name="inscript">Rugby</button>
       </form>
       <form method="post" action="natation.py">
       <button type="submit" name="inscript">Natation</button>
       </form>
       <form method="post" action="natation_artistique.py">
       <button type="submit" name="inscript">Natation artistique</button>
       </form>
      </div>
        <div>
       <form method="post" action="natation_marathon.py">
       <button type="submit" name="inscript">Natation marathon</button>
       </form>
       <form method="post" action="plongeon.py">
       <button type="submit" name="inscript">Plongeon</button>
       </form>
       <form method="post" action="waterpolo.py">
       <button type="submit" name="inscript">Waterpolo</button>
       </form>
       <form method="post" action="sports_equestres.py">
       <button type="submit" name="inscript">Sports équestres</button>
       </form>
       </div>
        <div>
       <form method="post" action="taekwondo.py">
       <button type="submit" name="inscript">Taekwondo</button>
       </form>
       <form method="post" action="tennis.py">
       <button type="submit" name="inscript">Tennis</button>
       </form>
       <form method="post" action="tennis_de_table.py">
       <button type="submit" name="inscript">Tennis de table</button>
       </form>
       <form method="post" action="tir.py">
       <button type="submit" name="inscript">Tir</button>
       </form>
      </div>
        <div>
       <form method="post" action="tir_a_l_arc.py">
       <button type="submit" name="inscript">Tir à l'arc</button>
       </form>
       <form method="post" action="triathlon.py">
       <button type="submit" name="inscript">Triathlon</button>
       </form>
       <form method="post" action="voile.py">
       <button type="submit" name="inscript">Voile</button>
       </form>
       <form method="post" action="volleyball.py">
       <button type="submit" name="inscript">Volleyball</button>
       </form>
     </div>
        <div>
       <form method="post" action="volleyball_de_plage.py">
       <button type="submit" name="inscript">Volleybal de plage</button>
       </form>
       <form method="post" action="breaking.py">
       <button type="submit" name="inscript">Breaking</button>
       </form>
       <form method="post" action="escalade_sportive.py">
       <button type="submit" name="inscript">Escalade sportive</button>
       </form>
       <form method="post" action="skateboard.py">
       <button type="submit" name="inscript">Skateboard</button>
       </form>
     </div>
        <div>
       <form method="post" action="surf.py">
       <button type="submit" name="inscript">Surf</button>
       </form>
       </div>
       </div>
</body>
</html>"""

print(html)