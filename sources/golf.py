# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 14:47:13 2024

@author: lbelair
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
    <link rel="stylesheet" href="Style/styles.css"/>
	<title>Les JO Paris 2024</title>
</head>
<body>
<h1>Le golf</h1>
<h3>Son histoire</h3>
<p>Le golf faisait partie du programme olympique des IIes et IIIes Olympiades, en 1900 et 1904. Plus de 
100 ans plus tard, le golf revient au programme des Jeux Olympiques, d’abord à Rio en 2016, puis à Tokyo 
en 2020.
 </p>
 <h3>Ces règles et informations</h3>
 <p>Dans son principe, le golf est un jeu simple, et assez efficacement décrit par la première de ses
 règles : « Le golf consiste à jouer une balle avec un club depuis l’aire de départ jusqu’au trou en la 
 frappant d’un ou plusieurs coups successifs conformément aux règles ». En fonction du type de terrain sur
 lequel se trouve la balle, le club utilisé par le joueur diffère.</br>
 La formule de jeu retenue pour les épreuves des Jeux Olympiques est celle du « stroke-play ». Il s’agit
 de comptabiliser le nombre de coups qu’un joueur doit effectuer pour arriver au bout du parcours de 18 
 trous ; le parcours doit être effectué quatre fois en quatre jours. Le joueur qui, à l’issue de ces quatre
 parcours, comptabilise le moins de coups joués, remporte le tournoi.
 <h3>Son pictogramme</h3>
<img src="Image/golf.webp"/>

</body>
</html>"""

print(html)